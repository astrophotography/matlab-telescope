function [Az,Alt]=xy2azalt(self, X,Y)
% XY2AZALT Convert Stereographic polar/pixel projection to Azimuthal coordinates.
%
%   XY2AZALT(S,X,Y) Compute Azimuth and Altitude [in deg] from [X,Y] 
%   stereographic polar/pixel projection (in -1:1 range).

  Az    =   atan2(Y, X)       *180/pi;
  Alt2  = 2*atan2(X, cosd(Az))*180/pi;
  %     = 2*atan2(Y, sind(Az))*180/pi;  
  %     = 2*atand(sqrt(X.^2+Y.^2));
  
  Alt   = 90-Alt2; % at Alt=90 (zenith) X=Y=0.
  Az    = Az-90; % Az=0 to North (X=0, Y=1)

end

function filename = annotate(self, filename, varargin)
% ANNOTATE Solve an image field.
%
%  ANNOTATE(S, filename, ...) Plate-solves given image filename.
%    Options can be given as [ 'name',value ] pairs:
%      ra:          approximate RA coordinate  (in hours)
%      dec:         approximate DEC coordinate (in deg)
%      radius:      approximate field size     (in deg)
%      scale-low:   lower estimate of the field coverage (in [deg], e.g. 0.1)
%      scale-high:  upper estimate of the field coverage (in [deg], e.g. 180)
%
%  ANNOTATE(S, filename, findobj('object_name')) Plate-solves around given object.
%
%  ANNOTATE(S, filename, ..., 'onAnnotated', callback) Triggers the given callback
%  when annotation succeeds.
%  ANNOTATE(S, filename, ..., 'AutoAlign', true|false) Triggers an alignement of 
%  the mount once annotated. When not specified, S.Callback.AutoAlign is used.
%
%  ANNOTATE(S) Updates all running processes (status, results, etc)
%
%  ANNOTATE(S, ACTION) Performs given action
%    'clear'    Clear largest files from annotation directories.
%    'image'    Generate all annotated plots for reports.
%    'list'     List status of running/past annotations.
%    'stop'     Stop all running processes.
%    'plot'     Plot all performed annotations, in separate figure.
%    'subplot'  Plot all performed annotations, in a single figure.
%    'update'   Update all running processes (status, results, etc)
%  ANNOTATE(S, S.Annotations(n), ACTION) Performs given action on specified annotation(s).

% this is where autoalign is handled (after annotate/update).

% we store results as an array of struct in self.Annotations

  if nargin == 1
    filename = 'update';
  end
  
  % ----------------------------------------------------------------------------
  % actions on past annotations
  
  if ischar(filename)
    switch filename
    case {'clear','plot','subplot','update','image','stop','list'}
      varargin{1}   = filename;
      filename      = self.Annotations;
      if isempty(filename), return; end
    end
  end
  
  % handle action on filename=Annotations
  if isstruct(filename) && (isfield(filename, 'process_java') || isfieldi(filename,'RA'))
    if numel(varargin) && ischar(varargin{1})
      action = varargin{1};
    else action = '';
    end
    filename = annotate_actions(self, filename, lower(action));
    return
  end % actions on running/past annotations

  % ----------------------------------------------------------------------------
  
  % store initial image structure (from Telescope.image) when given
  img_struct = [];
  if      isfield(filename, 'lastImageFile')
    img_struct = filename;
    filename = filename.lastImageFile; 
  else
    [tf,f]=isfieldi(filename, 'FileName');
    if tf
      img_struct = filename;
      filename   = filename.(f); 
    end
  end
  if ~ischar(filename) || ~exist(filename,'file')
    if ~ischar(filename), filename=''; end
    error([ mfilename ': filename ' filename ' does not exist.' ])
  end
  d = tempname;
  mkdir(d);
  
  this.autoalign    = self.Callback.AutoAlign;    % annotate -> align
  this.description  = '';
  this.dir          = d;
  this.exitValue    = [];
  this.filename     = filename;
  this.index        = numel(self.Annotations)+1;
  this.method       = 'solve-field (astrometry.net)';
  this.onAnnotated  = [];
  this.results      = [];
  this.status       = 'running';
  this.stderr       = '';
  this.stdout       = '';
  this.time_start   = clock;
  this.time_stop    = [];
  if ~isempty(img_struct)
    this.image = img_struct;
  else
    this.image = [];
  end
  
  % local solve-field
  self.private.executables = find_executables;
  
  cmd = [ self.private.executables.solve_field  ];
  cmd = [ cmd  ' '           filename ];
  cmd = [ cmd ' --dir '      d ];
  cmd = [ cmd ' --rdls '     fullfile(d, 'results.rdls') ];
  cmd = [ cmd ' --corr '     fullfile(d, 'results.corr') ' --tag-all' ];
  cmd = [ cmd ' --wcs '      fullfile(d, 'results.wcs') ];
  cmd = [ cmd ' --overwrite' ];
  
  % handle additional arguments in name/value pairs
  if nargin >= 3
    for f=1:numel(varargin)
      if isstruct(varargin{f}) && isfieldi(varargin{f}, 'RA') && isfieldi(varargin{f}, 'DEC')
        found = varargin{f};
        varargin{end+1} = 'ra';
        varargin{end+1} = isfieldi(found,'RA', 'value');
        varargin{end+1} = 'dec';
        varargin{end+1} = isfieldi(found,'DEC','value');
        varargin{end+1} = 'radius';
        varargin{end+1} = 5;
        varargin(f) = [];
      elseif strncmp(varargin{f}, 'onAnnot', 7) && f < numel(varargin)
        this.onAnnotated = varargin{f+1};
      elseif strcmpi(varargin{f}, 'AutoAlign')  && f < numel(varargin)
        this.autoalign   = varargin{f+1};
        varargin{f} = ''; varargin{f+1} = '';
      end
    end
    if mod(numel(varargin), 2) == 0
      for f=1:2:numel(varargin)
        if ischar(varargin{f}) && ~isempty(varargin{f}) && ~isempty(varargin{f+1})
          if strcmp(varargin{f}, 'ra')
            varargin{f+1}=varargin{f+1}*15; % h->deg
          end
          cmd = [ cmd ' --' lower(varargin{f}) '=' num2str(varargin{f+1}) ];
        end
      end
    end
  end

  % execute command
  if self.Verbosity > 0
    disp([ '[' datestr(now) '] Start ' mfilename ' ' filename ' in ' d ])
    disp(cmd)
  end
  this.command      = cmd;
  this.args         = varargin;
  
  % the command to launch may contain environment variables to bypass Matlab
  % libraries
  if ismac || isunix
    if ismac,      precmd = 'DYLD_LIBRARY_PATH=  ';
    elseif isunix, precmd = 'LD_LIBRARY_PATH='; end
    f = fullfile(d, 'start.sh');
    fid = fopen(f,'a+');
    if fid ~= -1
      fprintf(fid, '#!/bin/sh\n');
      fprintf(fid, 'export %s\n', precmd);
      fprintf(fid, '%s >>%s 2>&1\n', cmd, fullfile(d,'annotated.log'));
      fclose(fid);
      fileattrib (f, '+x'); % executable
      cmd = f;
    end
  end
  
  % launch a Java asynchronous command
  this.process_java = java.lang.Runtime.getRuntime().exec(cmd);
  if  isempty(self.Annotations)
    self.Annotations        = this;
  else
    self.Annotations(end+1) = this;
  end
  
  notify(self, 'ANNOTATION_START');
  
  filename = this;
end % annotate

% ------------------------------------------------------------------------------
function executables = find_executables
  % find_executables: locate executables, return a structure
  
  % stored here so that they are not searched for further calls
  persistent found_executables
  
  if ~isempty(found_executables)
    executables = found_executables;
    return
  end
  
  if ismac,      precmd = 'DYLD_LIBRARY_PATH= ;';
  elseif isunix, precmd = 'LD_LIBRARY_PATH= ; '; 
  else           precmd=''; end
  
  if ispc, ext='.exe'; else ext=''; end
  
  executables = [];
  this_path   = fullfile(fileparts(which(mfilename)));
  
  % what we may use
  for exe =  { 'solve-field' }
  
    for try_target={ ...
      fullfile(this_path, [ exe{1} ext ]), ...
      fullfile(this_path, [ exe{1} ]), ...
      [ exe{1} ext ], ... 
      exe{1} }
      
      if exist(try_target{1}, 'file')
        status = 0; result = 'OK';
      else
        [status, result] = system([ precmd try_target{1} ' --version' ]); % run from Matlab
      end
      
      name = strrep(exe{1}, '-','_');
      name = strrep(name  , '.','_');

      if status ~= 127
        % the executable is found
        executables.(name) = try_target{1};
        break
      else
        executables.(name) = [];
      end
    end
  
  end
  
  found_executables = executables;
  
end % find_executables



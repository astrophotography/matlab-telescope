function place = location(self, city)
  % LOCATION Guess the local geographic coordinates (latitude, longitude).
  %
  %   PLACE = LOCATION(S) Gets the [latitude longitude] in degrees, 
  %   from http://ip-api.com/json
  %
  %   PLACE = LOCATION(S, CITY_OR_COUNTRY) Gets the [latitude longitude] in degrees,
  %   for given CITY/COUNTRY. A dialogue window is shown in case there are multiple 
  %   choices.
  
  if nargin<2, city=[]; end
  place = [];
  
  % first check if we request a database search from
  % https://github.com/bahar/WorldCityLocations
  if ~isempty(city) && ischar(city)
    file = 'World_Cities_Location_table.csv';
    p    = fileparts(which(mfilename));
    fid  = fopen(fullfile(p,'private',file));
    if fid == -1
      error([ mfilename ': Can not find ' fullfile(p,'private',file) ]);
    end
    % "ID";"COUNTRY";"CITY";"LAT";"LONG";"ALT"\n
    vars = textscan(fid,'%u %s %s %f %f %f','Delimiter',';','Whitespace','"');
    fclose(fid);
    % now search
    countries = strrep(vars{2},'"','');
    cities    = strrep(vars{3},'"','');
    lat       = vars{5};
    long      = vars{6};
    index     = find(strcmpi(city,cities));
    if isempty(index)
      index = find(strcmpi(city,countries));
    end
    if numel(index) > 1
      ListString = strcat(countries, ' - ',  cities);
      index2 = listdlg('ListString', ListString, 'SelectionMode','single', ...
        'Name',[ class(self) ': Select ' city ' location' ]);
      if isempty(index2), return; end  
      index  = index(index2);
    end
    
    place = [ lat(index) long(index) ];
    if self.Verbosity > 0
      disp([ class(self) ': Selecting ' cities{index} ' ' countries{index} ' [long lat]=' mat2str(place) ' deg' ]);
    end
    self.Coord.Geographic.lat  = lat(index);
    self.Coord.Geographic.long = long(index);
    
  else % from IP
  
    % could also use: https://api.ipdata.co/
    % is network service available ?
    place = [];
    try
      ip = java.net.InetAddress.getByName('ip-api.com');
    catch
      return
    end
    if ip.isReachable(1000)
      ipj = urlread('http://ip-api.com/json');
      ip = parse_json(ipj);  % into struct (private)
      place = [ ip.lat ip.lon ];
      if self.Verbosity > 0
        disp([ class(self) ': You seem to be located near ' ip.city ' ' ip.country ' [long lat]=' mat2str(place) ' deg' ]);
      end
      self.Coord.Geographic.lat  = ip.lat;
      self.Coord.Geographic.long = ip.lon;
      self.Coord.Geographic.details = ipj;
    end
  end
end % end

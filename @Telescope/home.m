function home(self, arg);
% HOME Move mount to its home position, e.g. all angles at 0.
  slewto(self, self.Coord.Home);
end % home

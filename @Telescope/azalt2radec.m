function [RA Dec UTC JD] = azalt2radec(self, varargin)
% AZALT2RADEC Convert Azimuthal to Equatorial coordinates.
%
%   [RA,DEC]=AZALT2RADEC(S, AZ,ALT) Converts the [AZ,ALT] azimuthal to [RA,DEC]
%   equatorial coordinates, at telescope location and time.
%   The ALT and AZ angles are given in degrees, and as arrays for mass conversion.
%   The RA is returned in hours, and DEC in degrees.
%
%   [RA,DEC]=AZALT2RADEC(S, ..., TIME) Uses the given UTC TIME.
%   The TIME is the Universal Time Coordinated that can be given as a scalar
%   (e.g. now), a vector [yyyy mm dd HH MM SS] (e.g. clock), or a string (e.g.
%   'dd-mmm-yyyy HH:MM:SS' or 'yyyy-mm-dd HH:MM:SS'). 
%
%   [RA,DEC]=AZALT2RADEC(S, ..., Coord) Uses the given longitude/latitude
%   settings (as a struct with 'long','lat' fields).
%
%   Example: for Capella, should return RA='5:18:29' Dec='46°01:6'
%   s.Coord.Geographic=struct('lat',48,'long',2);
%   [RA Dec] = azalt2radec(s, 37.11, 16.18, '22-Feb-2024 10:49:19');
%   { angle2hms(s, RA,'hms') angle2hms(s, Dec, 'dms') }
%
% See also: radec2azalt, radec2xy

  RA=[]; Dec=[]; UTC=[]; JD=[];
  az=[]; alt=[]; time=[];
  Coord = self.Coord.Geographic;
  flag_isEq = false;
  
  % handle input arguments (struct, char, num)
  index=1;
  while index<=numel(varargin)
    s=varargin{index};
    if isstruct(s) && (isfieldi(s, 'alt') || isfieldi(s, 'az'))
      [tf,f]=isfieldi(s, 'alt'); if tf, alt =s.(f); end
      [tf,f]=isfieldi(s, 'az');  if tf, az  =s.(f); end
    elseif isstruct(s) && (isfield(s, 'long') && isfield(s, 'lat'))
      Coord = s;
    elseif ischar(s)
      switch s
      case 'az'
        if index < numel(varargin), az =varargin{index+1}; index=index+1; end
      case 'alt'
        if index < numel(varargin), alt=varargin{index+1}; index=index+1; end
      case 'time'
        if index < numel(varargin), time=varargin{index+1}; index=index+1; end
      case 'equatorial'
        Coord = self.Coord.Geographic;
        flag_isEq = true;
      case 'azimuthal'
        Coord = self.Coord.Geographic;
      otherwise
        time = s; % time as char
      end
    elseif isnumeric(s) && isempty(az)
      az = s;
    elseif isnumeric(s) && isempty(alt)
      alt= s;
    elseif isnumeric(s) && isempty(time) && any(numel(s) == [1 6])
      time=s;
    end
    index=index+1;
  end

  % handle default input
  if isempty(az),  az =self.Coord.Horizontal.az;  end
  if isempty(alt), alt=self.Coord.Horizontal.alt; end
  
  if flag_isEq % when Coord = equatorial
    RA = az/15;
    Dec= alt;
    return
  end
  
  % compute Local Sidereal Time
  [UTC, JD, lst]=date(self, time, Coord); % lst in hours = f(longitude)
  
  lat = Coord.lat;
  Dec = asind( sind(alt).*sind(lat) + cosd(alt).*cosd(lat).*cosd(az) );
  
  % compute local hour angle (LHA)
  LHA = atan2( -sind(az).*cosd(alt).*cosd(lat) , ...
              ( sind(alt)-sind(Dec).*sind(lat) ) )*180/pi;
  
  % lst = gmst+long
  RA  = mod(lst*15-LHA,360)/15; % deg->hours
  
end


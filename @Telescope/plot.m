function [handles, fig, object_list] = plot(self, varargin)
  % PLOT Plot a sky-chart centered on RA/Dec or the current Telescope location.
  %
  %   PLOT(S) Plots an image of the Telescope field of view
  %
  %   PLOT(S, RA, DEC) Plots an image of the Telescope field at RA/DEC.
  %   The RA must be given in hours. The DEC must be given in degrees.
  %   PLOT(S, 'ra',RA, 'dec',DEC) is equivalent.
  %
  %   PLOT(S, 'NAME') Plots an image of the Telescope field at coordinates 
  %   of given object.
  %
  %   PLOT(S, ..., FOV) Plots an image of the Telescope field with given
  %   field-of-view in degrees. PLOT(S, 'fov', FOV) is equivalent.
  %
  %   PLOT(..., 'nolabels') Same as above, and hides object labels.
  %
  %   PLOT(..., 'figure',fig) Same as above, and use given figure.
  %
  %   [HANDLES, FIG, OBJECT_LIST] = PLOT...) Returns plots and figure handles, 
  %   as well as a list of objects on plot.
  %
  %   The plot shows the following items:
  %   - stars
  %   - deep sky objects
  %   - constellations
  %   - planets
  %   - Zenith, Pole, current scope location, goto Target, last Annotation

  persistent flag_below_horizon
  
  if isempty(flag_below_horizon) flag_below_horizon=false; end
  handles = []; fig = []; object_list=[];
  show_labels = true; ra=[]; dec=[]; field_of_view=[];
  Coord = self.Coord.Geographic;
  
  % handle input arguments
  n = numel(varargin);
  i = 1;
  while i<=n
    s=varargin{i};
    if     strcmpi(s, 'ra' ) && i < n,    ra =varargin{i+1}; i=i+1;
    elseif strcmpi(s, 'dec') && i < n,    dec=varargin{i+1}; i=i+1;
    elseif strcmpi(s, 'figure') && i < n, fig=varargin{i+1}; i=i+1;
    elseif strcmpi(s, 'fig') && i < n,    fig=varargin{i+1}; i=i+1;
    elseif strcmpi(s, 'fov') && i < n,    field_of_view=varargin{i+1}; i=i+1;
    elseif strcmpi(s, 'nolabels') show_labels= false;
    elseif ischar( s)
      found = findobj(self, s);
      if ~isempty(found) && isfieldi(found,'ra')
        s = found;
      end
    elseif isnumeric(s)
      if     isempty(ra),  ra = s;
      elseif isempty(dec), dec= s;
      elseif isempty(field_of_view), field_of_view=s;
      elseif ishandle(s) && isempty(fig),  fig= s;
      end
    elseif ishandle(s) && isempty(fig),    fig= s;
    end
    if isstruct(s)
      [tf,f]=isfieldi(s, 'ra');  if tf, ra  =s.(f); end
      [tf,f]=isfieldi(s, 'dec'); if tf, dec =s.(f); end
      if isfield(s, 'long') && isfield(s, 'lat')
        Coord = s;
      end
    end
    i=i+1;
  end
  
  % default value
  if isempty(ra) || isempty(dec)
    ra = self.Coord.Equatorial.ra;  % in [h]
    dec= self.Coord.Equatorial.dec; % in [deg]
  end
  if isempty(field_of_view)
    field_of_view = 10*self.Rate.Slew; % in [deg]
  end
  
  % ----------------------------------------------------------------------------
  % compute FoV and update planets
  
  % get field of view and maximum magnitude to display
  field_of_view_xy = field_of_view/180; % in stereographic projection
  mag_max          = abs(2.5*log10(abs(4/field_of_view_xy)))+7;
  
  % update planets
  place = [ self.Coord.Geographic.long self.Coord.Geographic.lat ];
  self.catalogs.planets = getcatalogs_planets(self.Time.Julian, place, self.Time.LST);

  % compute scope location distance to center in stereographic coordinates
  [x0,y0, Az, Alt]  = radec2xy(self, ra, dec, Coord); % we use here the theoretical coordinates
  if Alt < 0
    if ~flag_below_horizon
      warning([ mfilename ': The target  RA=' angle2hms(self, ra, 'hms') ...
        ' DEC=' angle2hms(self, dec,'dms') ' may be below the horizon.' ]);
    end
    flag_below_horizon = true;
  else
    flag_below_horizon = false;
  end
  
  if field_of_view_xy <= 1e-2; field_of_view_xy=0.05; end
  if field_of_view_xy>=1
    xmin=-1; xmax=1; ymin=-1; ymax=1;
  else
    xmin=x0-field_of_view_xy/2;
    xmax=x0+field_of_view_xy/2;
    ymin=y0-field_of_view_xy/2;
    ymax=y0+field_of_view_xy/2;
  end
  
  % open the Figure ------------------------------------------------------------
  if isempty(fig)
    fig = findall(0, 'Tag', [ 'Telescope_Plot_' self.uid ]);
  end
  if isempty(fig)
    fig = figure('Tag', [ 'Telescope_Plot_' self.uid ], ...
      'NumberTitle','off', 'Renderer', 'Zbuffer', 'InvertHardcopy','off');
    p = get(fig, 'Position');
    p(3:4) = max(p(3:4));
    set(fig, 'Position', p);
  else
    ht = findall(fig, 'Tag',[ 'Telescope_Plot_Labels_' self.uid ]);
    if ~isempty(ht) && strcmp(get(ht(1),'Visible'), 'off')
      show_labels = false;
    end
    set(0, 'CurrentFigure', fig);
    clf;
  end

  name = [ 'RA='  angle2hms(self, ra, 'hms') ...
          ' DEC=' angle2hms(self, dec,'dms') ...
          ' FoV=' num2str(field_of_view) ' [deg]: ' class(self) ];
  set(fig, 'Name', name);
  object_list = { datestr(now) name };
  object_str  = { datestr(now) name };
  
  % compute FontSize from DPI, for labels.
  dpi      = max(get(0,'screensize'))/13; % 13" screen
  FontSize = max(get(0,'DefaultUicontrolFontSize'), 12*dpi/144);
  if FontSize < 8, FontSize=8; elseif FontSize > 24, FontSize=24; end
  
  % ----------------------------------------------------------------------------
  % plot marks (+)
  
  % show center RA/Dec location
  hold on
  ht = [ plot(x0, y0, 'r+', 'MarkerSize', FontSize) ...
          text(x0, y0, [ ' ' num2str(ra,4) ' ' num2str(dec,4) ], 'Color','r', 'FontSize', FontSize) ];
  % plot Target when aside
  if isfield(self.Coord.Target,'ra')
    [x3,y3] = radec2xy(self, self.Coord.Target.ra, self.Coord.Target.dec, Coord);
    if ((x0-x3)^2+(y0-y3)^3) > 1e-4 % show Target and a dashed-line to it
      ht = [ ht line([x0 x3],[y0 y3], 'LineStyle','--','Color','r') ];
      ht = [ ht plot(x3, y3, 'rx', 'MarkerSize', FontSize) ...
            text(x3, y3,'Target', 'Color','r', 'FontSize', FontSize) ];
    end
  end
  % plot Solved location when available, and a dashed-line to it
  if isstruct(self.Coord.Annotation) && isfield(self.Coord.Annotation, 'ra') ...
    && ~isempty(self.Coord.Annotation.ra)
    [x3,y3] = radec2xy(self, self.Coord.Annotation.ra, self.Coord.Annotation.dec, Coord);
    ht = [ ht plot(x3, y3, 'c+', 'MarkerSize', FontSize) ];
    if ((x0-x3)^2+(y0-y3)^3) > 1e-4 % show a dashed-line to it
      ht = [ ht line([x0 x3],[y0 y3], 'LineStyle','--','Color','c') ];
      ht = [ ht text(x3, y3,'Ann', 'Color','c', 'FontSize', FontSize) ];
    end
  end
  
  % show North/South Pole
  if dec >= 0
    [x1,y1]  = radec2xy(self, 0,  90, Coord);
    ht = [ ht plot(x1, y1, 'c+', 'MarkerSize', FontSize) ...
            text(x1, y1,'North Pole', 'Color','c', 'FontSize', FontSize) ];
  else 
    [x2,y2]  = radec2xy(self, 0, -90, Coord);
    ht = [ ht plot(x2, y2, 'c+', 'MarkerSize', FontSize) ...
            text(x2, y2,'South Pole', 'Color','c', 'FontSize', FontSize) ];
  end
  % add Zenith
  ht = [ ht plot(0, 0, 'c+', 'MarkerSize', FontSize) ...
          text(0, 0,'Zenith', 'Color','c', 'FontSize', FontSize) ];
  save toto.mat ht
  set(ht, 'Tag', [ 'Telescope_Plot_Labels_' self.uid ]);
  
  % ----------------------------------------------------------------------------
  % plot items from catalogs

  for f=fieldnames(self.catalogs)'
  
    catalog = self.catalogs.(f{1});
    h = [];
    if strcmp(f{1}, 'constellations') 
      h = plot_constellations(self, catalog, Coord);
                
      set(h, 'Tag', [ 'Telescope_Plot_Labels_' self.uid ]);
    else
    
      % compute X,Y for these
      [catalog.X,catalog.Y, Az1, Alt1] = radec2xy(self, catalog.RA, catalog.DEC, Coord); % this is fast
      
      % restrict within scope range, quadrant, magnitude and horizon
      if field_of_view_xy>=1
        if strcmp(f{1}, 'deep_sky_objects'), continue; end
      end
      index = find(xmin < catalog.X & catalog.X < xmax ...
                        & ymin < catalog.Y & catalog.Y < ymax ...
                        & catalog.X.^2 + catalog.Y.^2 < 1 & catalog.MAG < mag_max);
      if isempty(index), continue; end
      catalog = sort_catalog(catalog, index);
      
      % now sort by magnitude
      [~, index] = sort(catalog.MAG);
      if numel(index) > 100, index=index(1:100); end
      catalog = sort_catalog(catalog, index); 
      
      c = print_catalog(f{1}, catalog);
      object_str = { object_str{:} c{:} };
      
      object_list{end+1} = catalog;
      
      % plot each 'catalog' category with given 'symbol'
      h = plot_xy(f{1}, catalog.X, catalog.Y, catalog.MAG,  catalog.TYPE, ...
                        catalog.SIZE);
      if strcmp(f{1}, 'deep_sky_objects')
        set(h, 'Tag', [ 'Telescope_Plot_Labels_' self.uid ]);
      end
      
      % we tag the 'n' brightest objects for each category
      n = 10;
      if numel(catalog.X) > n
        index=1:n;
        catalog = sort_catalog(catalog, index);
      end
      for index=1:numel(catalog.NAME)
        ht = text(catalog.X(index),catalog.Y(index), catalog.NAME{index},  ...
          'Color','y', 'FontSize', FontSize);
        set(ht, 'Tag',[ 'Telescope_Plot_Labels_' self.uid ]);
      end

    end
    handles = [ handles h ];

  end % for
  
  % ----------------------------------------------------------------------------
  % labels etc
  
  object_str = sprintf('%s\n', object_str{:});
  set(gcf, 'UserData', object_str);
  set(gca, 'Color',    'k'); % black background
  set(gca, 'XTickLabel', {}, 'YTickLabel', {}, 'Position', [0 0 1 1]);
  xlim(gca, sort([ xmin xmax ]));
  ylim(gca, sort([ ymin ymax ]));
  hold off
  
  % attach contextual menu to show/hide labels
  m = uicontextmenu('Parent', fig);
  uimenu(m, 'Label', '<html><b>Field center</b></html>');
  uimenu(m, 'Label', [ 'RA='  angle2hms(self, ra, 'hms') ]);
  uimenu(m, 'Label', [ 'DEC=' angle2hms(self, dec,'dms') ]);
  uimenu(m, 'Label', [ 'FoV=' num2str(field_of_view) ' [deg]' ]);
  uimenu(m, 'Label','Show labels', 'Callback',[ 'set(findall(gca, ''Tag'',''Telescope_Plot_Labels_' ...
    self.uid '''),''visible'',''on'')' ], 'Separator','on');
  uimenu(m, 'Label','Hide labels', 'Callback',[ 'set(findall(gca, ''Tag'',''Telescope_Plot_Labels_' ...
    self.uid '''), ''visible'',''off'')' ]);
  uimenu(m, 'Label','Copy Objects to clipboard', 'Callback', [ ...
    'clipboard(''copy'',get(gcbf, ''UserData''));' ]);
  
  set(gca,'xtick',[],'ytick',[]);
  set(gcf, 'UIContextMenu', m);
  set(gca, 'UIContextMenu', m);
  
  pbaspect([1 1 1]); % IMPORTANT: aspect ratio must be 1.
  
  ht = findall(fig, 'Tag', [ 'Telescope_Plot_Labels_' self.uid ]);
  set(ht, 'UIContextMenu', m);
  if ~show_labels
    set(ht, 'visible','off');
  end
  
end % plot

% ------------------------------------------------------------------------------

% ------------------------------------------------------------------------------

function h = plot_constellations(self, constellations, Coord)
  % PLOT_CONSTELLATIONS Plot constellation lines and labels
  
  % compute_constellations: compute Alt-Az coordinates for constellations
  [Az1, Alt1] = radec2azalt(self, constellations.Lines_RA1, constellations.Lines_DEC1, Coord);
  [Az2, Alt2] = radec2azalt(self, constellations.Lines_RA2, constellations.Lines_DEC2, Coord);
  [Az,  Alt]  = radec2azalt(self, constellations.RA, constellations.DEC, Coord);
  names = constellations.NAME;

  % and XY
  [X1, Y1] = azalt2xy(self, Az1, Alt1);
  [X2, Y2] = azalt2xy(self, Az2, Alt2);
  [X3, Y3] = azalt2xy(self, Az,  Alt);
  
  % identify the visible Constellations and plot
  v  = ((X1.*X1+Y1.*Y1 < 1 | X2.*X2+Y2.*Y2 <1) & (Alt1 > 0 | Alt2 > 0));
  X1 = X1(v); Y1 = Y1(v); 
  X2 = X2(v); Y2 = Y2(v);
  
  X = nan*ones(numel(X1)*3, 1); Y=X;
  X(1:3:(end-2)) = X1; X(2:3:(end-1)) = X2; 
  Y(1:3:(end-2)) = Y1; Y(2:3:(end-1)) = Y2; 
  h = line(X,Y, 'Color','g','LineWidth',1);
  hold on
  set(h, 'Tag', [ 'Telescope_Plot_Labels_ ' self.uid ]);

  %--- Plot Constellation Names ---
  % compute FontSize from DPI, for labels.
  dpi      = max(get(0,'screensize'))/13; % 13" screen
  FontSize = max(get(0,'DefaultUicontrolFontSize'), 8*dpi/72);
  
  ht = [];
  for Icn=1:length(X3)
     x = X3(Icn);
     y = Y3(Icn);
     if x*x+y*y > 1, continue; end
     Htext   = text(x, y, names{Icn}, ...
                   'FontSize', FontSize, 'Color','g');
     ht = [ ht Htext ];
  end
  
  % Horizon
  Theta = (0:5:360)';
  X     = cosd(Theta);
  Y     = sind(Theta);
  ht = [ ht plot(X,Y, 'LineWidth',3) ];
  
  % NSEW labels
  Offset = 0.02;
  Letter = 0.05;
  h1 = text(0,                1+Offset,       'N', 'Color','b', 'FontSize', FontSize);
  h2 = text(-1-Offset-Letter, 0,              'E', 'Color','b', 'FontSize', FontSize);
  h3 = text(0,               -1-Offset-Letter,'S', 'Color','b', 'FontSize', FontSize);
  h4 = text(1+Offset,         0,              'W', 'Color','b', 'FontSize', FontSize);
  ht = [ ht h1 h2 h3 h4 ];
  set(ht, 'Tag', [ 'Telescope_Plot_Labels_' self.uid ]);

end % plot_constellations

function c = print_catalog(cname, catalog)
% PRINT_CATALOG Prints elements from the given catalog.
  c = {};
  for index=1:numel(catalog.RA)
    c{end+1} = sprintf('%17s %8.4f %8.4f %5.2f %s', cname, ...
      catalog.RA(index), catalog.DEC(index), catalog.MAG(index), catalog.NAME{index});
  end
end % print_catalog

% ------------------------------------------------------------------------------

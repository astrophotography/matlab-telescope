function found = findobj(self, name)
      % FINDOBJ Search for an object (star, DSO, planet) in catalogs.
      %
      %   FINDOBJ(S, NAME) Searches for a given object in catalogs. 
      %   The found object is returned as a structure with RA[h] and DEC[deg].
      %
      %   FINDOBJ(S) Opens a Dialogue to enter the name of an object to search.
      
      % update planet positions
      place = [ self.Coord.Geographic.long self.Coord.Geographic.lat ];
      self.catalogs.planets = getcatalogs_planets(self.Time.Julian, place, self.Time.LST);
      
      catalogs = fieldnames(self.catalogs);
      found = [];
      
      if nargin < 2
        prompt = { ['{\color{blue}Enter a Star/Object Name} e.g. Betelgeuse, M 42, NGC 224, Venus.'  ...
          'Use spaces between Catalog Name and ID.'  ...
          'Known Catalogs include: Planets, StarID, HD, HR, Messier, NGC, IC, ...' ] };
        name = 'Telescope: Find Object';
        options.Resize='on';
        options.WindowStyle='normal';
        options.Interpreter='tex';
        answer=inputdlg(prompt,name, 1, {'M 42'}, options);
        if ~isempty(answer), name = answer{1}; 
        else return; end
      end
      
      % check first for name without separator
      if ~any(name == ' ')
        [n1,n2]  = strtok(name, '0123456789');
        found = findobj(self, [ n1 ' ' n2 ]);
        if ~isempty(found) return; end
      end
      
      namel= strtrim(lower(name));
      for f=catalogs(:)'
        catalog = self.catalogs.(f{1});
        if ~isfield(catalog, 'RA') || ~isfield(catalog, 'NAME'), continue; end
        NAME = lower(catalog.NAME);
        % search for name
        index = find(~cellfun(@isempty, strfind(NAME, namel)));
        if numel(index > 1)
          % name appears in multiple places (as part of NAME)
          index2 = find(~cellfun(@isempty, strfind(NAME(index), [ ';' namel ';' ])));
          if isempty(index2)
          index2 = find(~cellfun(@isempty, strfind(NAME(index), [ namel ';' ])));
          end
          if isempty(index2)
          index2 = find(~cellfun(@isempty, strfind(NAME(index), [ ';' namel ])));
          end
          if ~isempty(index2)
            index = index(index2);
          end
        end
        if ~isempty(index)
          found.index   = index(1);
          found.catalog = f{1};
          found.RA      = catalog.RA(found.index);
          found.DEC     = catalog.DEC(found.index);
          found.NAME    = catalog.NAME{found.index};
          if isfield(catalog,'MAG')  found.MAG     = catalog.MAG(found.index);  end
          if isfield(catalog,'TYPE') found.TYPE    = catalog.TYPE{found.index}; end
          if isfield(catalog,'DIST') found.DIST    = catalog.DIST(found.index); end
          break;
        end
      end

      if ~isempty(found)
        disp([ mfilename ': Found object "' name '" as: ' found.NAME ' (in ' found.catalog ')' ])
        switch found.catalog
        case {'deep_sky_objects','stars'}, un='ly'; found.DIST = found.DIST*3.26;
        case 'planets', un='a.u.';
        otherwise, un='??';
        end
        if self.Verbosity > 0 && isfield(found,'DIST') && isfield(found,'MAG') && isfield(found,'TYPE')
          if found.DIST > 0
            disp(sprintf('  %s: Magnitude: %.1f ; Type: %s ; Dist: %.3g [%s]', ...
              found.catalog, found.MAG, found.TYPE, found.DIST, un ));
          else
            disp(sprintf('  %s: Magnitude: %.1f ; Type: %s', ...
              found.catalog, found.MAG, found.TYPE ));
          end
        end
        ra  = found.RA;
        dec = found.DEC;
        ras = angle2hms(self, ra, 'hms');
        decs= angle2hms(self, dec,'dms');
        [az, alt] = radec2azalt(self, ra, dec);
        if self.Verbosity > 0
          disp([ '  RA=' ras ' (' num2str(ra,3) ' h);   Dec=' decs ' (' num2str(dec,3) ' deg).' ]);
          disp([ '  Az=' num2str(az,3) ' deg;               Alt='  num2str(alt,3) ' deg.' ]);
        end
      else
        disp([ mfilename ': object ' name ' was not found.' ])
      end
      
    end

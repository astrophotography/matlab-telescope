function ang = hms2angle(self, h,m,s)
  % HMS2ANGLE Convert hh:mm:ss to an angle in [hour or deg].
  % 
  %   HMS2ANGLE(S, H,M,S) convert HH:MM:SS to an hour/angle. The nature of the result
  %   depends on the unit for 'H'.
  %
  %   HMS2ANGLE(S, HMS) Where HMS is a Right Ascension string e.g. HH:MM:SS in 
  %   which case the result is in hours.
  %
  %   HMS2ANGLE(S, DMS) does the same for a Declination string e.g. DD°MM'SS", 
  %   DD*MM'SS'' to get an angle in degrees.
  
  if nargin == 2 && isnumeric(h) && numel(h) == 3
    m = h(2); s=h(3); h=h(1);
  elseif nargin == 2 && ischar(h)
    hms = repradec(h);
    h=hms(1); m=hms(2); s=hms(3);
  end
  h   = double(h);
  ang = (abs(h) + double(m)/60 + double(s)/3600);
  if sign(h), ang=ang*sign(h); end
end % hms2angle

function str = repradec(str)
  %repradec: replace string stuff and get it into num
  str = strtrim(lower(str));
  for rep = {'h','m','s',':','°','deg','d','''','"','*','[',']'}
    str = strrep(str, rep{1}, ' ');
  end
  str = str2num(str);
end

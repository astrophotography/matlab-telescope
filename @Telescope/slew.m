function g=slew(self, varargin)
% SLEW Slew/move mount to given direction or coordinates, not affecting GOTO target.
%
%   SLEW(S, 'N'|'S'|'E'|'W') Slews to North/South/East/West. Use stop(S)
%   to stop the motion. This is equivalent to SLEW(S,'ax1|2',+/-Inf).
%
%   SLEW(S, 'ra', RA, 'dec',DEC)  Slews mount to given coordinates. 
%   SLEW(S, 'az', AZ, 'alt',ALT)  The Coord.Target is not affected.
%   SLEW(S, 'ax1',AX1,'ax2',AX2)
%
%   SLEW(S, 'M 51') Slew mount to a named object.

% TODO: should check that status is IDLE or TRACKING, not CAPTURING

  % handle input arguments
  if nargin == 1
    g = self.Slew_Target;
    return
  end
  
  % handle arguments
  [ra,dec,az,alt,ax1,ax2,name] = parseparams(self, varargin{:});
  
  % get coordinates to target
  if ~isempty(az) && ~isempty(alt) && isempty([ ra dec ])
    [ra, dec] = azalt2radec(self, az,alt);
  end
  if ~isempty(ax1) && ~isempty(ax2) && isempty([ ra dec ]) ...
  && all(isfinite([ax1 ax2]))
    [ra, dec] = azalt2radec(self, ax1,ax2);
  elseif ~isempty(ra) && ~isempty(dec) 
    % compute the mount coordinates, in case this is not azimuthal/equatorial
    [ax1, ax2]= radec2azalt(self, ra, dec);
  end
  
  if isempty(ax1) && isempty(ax2)
    warning([ mfilename ': ' mfilename ': no target given.' ]);
    return
  end
  
  % set Target
  g = self.Slew_Target;
  g.ra  = ra;
  g.dec = dec;
  g.active = true;
  if ~isempty(az) && ~isempty(alt)
    g.az =az;
    g.alt=alt;
  end

  if ~isempty(ax1), g.ax1=ax1;  end
  if ~isempty(ax2), g.ax2=ax2;  end
  if ~isempty(name)
    g.name = name;
    name = [ ' > ' name ];
  end

  self.Slew_Target  = g;
  notify(self,'MOTION_START');  % STOP in TimerCallback
  % MOTION_STOP is handled in the timer loop.
  
  if self.Rate.Slew < self.Rate.Slew_Levels(2)
    speed(self, self.Rate.Slew_Levels(2));
  end
  
  % Trigger the move
  % In pratice axis1 ~ RA or Az and axis2 ~ Dec or Alt
  slewaxis(self, 'ra', ra, 'dec', dec, 'az', az, 'alt', alt, 'ax1', ax1, 'ax2', ax2);

end % slew

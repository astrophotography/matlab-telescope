function display(self)
  % DISPLAY Display Telescope object.
  
  if ~isempty(inputname(1))
    iname = inputname(1);
  else
    iname = 'ans';
  end
  if isdeployed || ~usejava('jvm') || ~usejava('desktop') || nargin > 2, id=class(self);
  else id=[  '<a href="matlab:doc ' class(self) '">' class(self) '</a> ' ...
             '(<a href="matlab:methods ' class(self) '">methods</a>,' ...
             '<a href="matlab:inputdlg(' iname ');">keypad</a>,' ...
             '<a href="matlab:plot(' iname ');">plot</a>,' ...
             '<a href="matlab:disp(' iname ');">more...</a>)' ];
  end
  fprintf(1,'%s = %s %s\n',iname, id, char(self));
end % display

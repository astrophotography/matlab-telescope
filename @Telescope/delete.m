function delete(self) 
% DELETE Telescope object delete
  if isvalid(self)
    if ~isempty(self.telescope_timer) && isvalid(self.telescope_timer)
      stop(self.telescope_timer);
      delete(self.telescope_timer);
    end
    if ~isempty(self.figure_handle)
      delete(self.figure_handle);
    end
  end
end

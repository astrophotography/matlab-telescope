function event(src, evnt)
  % EVENT Update status/view from timer event.
  %
  % This event loop sets the 'status' which is returned by getstatus.
  %   status      | event                             | src.Callback
  % --------------|-----------------------------------|----------------------
  %   IDLE        | UPDATED                           | onUpdate(src)
  %   TRACKING    | UPDATED                           | onUpdate(src)
  %   GOTO        | GOTO_START        GOTO_STOP       | onGotoReached(src)
  %   SLEWING     | MOTION_START      MOTION_STOP     | onSlewToReached(src)
  %               | ANNOTATION_START  ANNOTATION_STOP | onAnnotated(src, ann)
  %   CAPTURING   | CAPTURE_START     CAPTURE_STOP    | onCaptured(src, img)
  %
  % Handles the status change and associated event notifications.
  %
  % Callbacks are stored in the 'Callback' as a structure.
  % Each callback can be given as a string or as a function handle which first 
  % argument is the Telescope object. The syntax is:
  %   - onUpdate(self)
  %   - onGotoReached(self)
  %   - onCaptured(self,  image_struct)
  %   - onAnnotated(self, annotation_struct)
  %
  % The 'image_struct' and 'annotation_struct` are structures with members
  %   filename
  %   status
  %   time_start
  %   time_stop
  %   results
  
  % callback        | set by      | triggered from 
  % ----------------|-------------|--------------------------------------------
  % onUpdate        | user        | event
  % onGotoReached   | user        | event_goto
  % onCaptured      | user,image  | event_capture
  % onAnnotated     | user,image  | event_annotate
  
  persistent last_ra last_dec last_ax1 last_ax2 last_az last_alt last_switch;

  if isempty(last_ra) || isempty(last_dec)
    last_ra  = Inf;
    last_dec = Inf;
    last_ax1 = nan;
    last_ax2 = nan;
    last_alt = nan;
    last_az  = nan;
    last_switch = nan;
  end
  
  if isa(src, 'timer')
    self = get(src, 'UserData');  % timer.UserData
  elseif isa(src, 'Telescope')
    self = src;
  end
  if ~isvalid(self) || isempty(self)
    if isa(evnt,'timer')
      stop(evnt);
      delete(evnt); 
    end
    return; 
  end
  
  status_prev = self.status;
  getstatus(self);          % update Telescope status (e.g. Coordinates)
  notify(self, 'UPDATED');  % we just updated
  c     = char(self);
  status= '';
  
  % Goto/Slew ------------------------------------------------------------------
  
  % check if we reach a GOTO Target
  if isfield(self.Coord.Target,'active') && self.Coord.Target.active
  
    status = event_goto(self, status);

  % check if we reach a SLEWTO (along Ax1/Ax2, goto without changing Coord.Target)
  elseif isfield(self.Slew_Target,'active') && self.Slew_Target.active
  
    status = event_slew(self, status, last_ax1, last_ax2);
  
  elseif isempty(self.Rate.Track) || any(strcmpi(self.Rate.Track,{'none','track_none'}))
    status = 'IDLE';
  else
    status = 'TRACKING';
  end
  
  if any(strncmp(status,      {'IDLE','TRAC'}, 4)) ...
  && any(strncmp(status_prev, {'SLEW','GOTO'}, 4))
    if self.Verbosity > 1
      disp([ '[' datestr(now) '] ' class(self) ': MOTION_STOP' ]);
    end
    notify(self, 'MOTION_STOP');
  end
  
  % Image capture and auto-annotation --------------------------------------------
  
  status = event_capture(self, status, status_prev); % execute onCaptured

  self.status = status;
  
  % update plot when openned or moved
  if ~isempty(findall(0, 'Tag', [ 'Telescope_Plot_' self.uid ])) ...
  && any([ abs(last_ra-self.Coord.Equatorial.ra)*15 abs(last_dec-self.Coord.Equatorial.dec) ] ...
           > self.Rate.Slew_Levels(1) )
    plot(self);
  end

  % Annotation -----------------------------------------------------------------
  
  % check if annotation ended (onAnnotated).
  event_annotate(self); % update running annotations, execute onAnnotated
  
  % update inputdlg figure -----------------------------------------------------
  
  last_switch = event_inputdlg(self, last_switch, last_ra, last_dec, last_az, last_alt);

  % execute specific subclass timerfcn
  event_evalfun(mfilename, self.Callback.onUpdate, self);
  
  last_ra = self.Coord.Equatorial.ra;
  last_dec= self.Coord.Equatorial.dec;
  last_az = self.Coord.Horizontal.az;
  last_alt= self.Coord.Horizontal.alt;
  last_ax1= self.Coord.Horizontal.ax1;
  last_ax2= self.Coord.Horizontal.ax2;

end % TimerCallback


% ------------------------------------------------------------------------------


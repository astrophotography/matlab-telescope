function reset(self)
  % RESET Reset the mount to its start-up state. 
  if self.Verbosity > 0
    disp([ '[' datestr(now) '] ' class(self) ': reset' ]);
  end
  home(self); pause(2);
  stop(self); pause(2);
  start(self);
end % reset

function [RA, Dec, Az, Alt]=xy2radec(self, x,y, varargin)
% XY2RADEC Convert Stereographic polar/pixel projection to Equatorial coordinates.
%
%   [RA,DEC]=XY2RADEC(S, X,Y) Converts the [X,Y] stereographic polar projection 
%   (in -1:1 range) to [RA,DEC] equatorial, at telescope location and time.
%
%   The RA is returned in hours, and DEC in degrees.
%
%   [RA,DEC]=XY2RADEC(S, X,Y, TIME) Uses the given UTC TIME.
%   The TIME is the Universal Time Coordinated that can be given as a scalar
%   (e.g. now), a vector [yyyy mm dd HH MM SS] (e.g. clock), or a string (e.g.
%   'dd-mmm-yyyy HH:MM:SS' or 'yyyy-mm-dd HH:MM:SS'). 
%
%   [RA,DEC]=XY2RADEC(S, ..., Coord) Uses the given longitude/latitude
%   settings (as a struct with 'long','lat' fields).
%
%   [RA,DEC, AZ,ALT]=XY2RADEC(...) Also returns azimuthal coordinates.
%
% See also: radec2azalt, azalt2radec

  [Az, Alt] = xy2azalt(self, x,y);
  [RA, Dec] = azalt2radec(self, Az, Alt, varargin{:});
end

function [X,Y]=azalt2xy(self, Az,Alt)
% AZALT2XY Project Azimuthal coordinates using the Stereographic polar projection.
%
%   AZALT2XY(S, AZ,ALT) Projects Azimuth and Altitude [in deg] to [X,Y] 
%   stereographic polar projection (in -1:1 range).

% 
  Alt2  = 90-Alt; % at Alt=90 (zenith) X=Y=0.
  Az    = Az+90; % Az=0 to North (X=0, Y=1)
  X     = cosd(Az).*tand(0.5.*Alt2);
  Y     = sind(Az).*tand(0.5.*Alt2);

end

function h = inputdlg(self, dt)
% INPUTDLG Display a key pad to control the mount.
%
%   INPUTDLG(S) Displays a control-pad with NSEW keys. Also restarts the 
%   monitoring timer.
%
%   INPUTDLG(S, DT) Sets the refresh rate. DT=0 stops periodic refresh.

  if nargin < 2,  dt=[];
  else
    self.telescope_timer_period = dt;
  end % default timer period: 2 s
  if isempty(dt), dt=self.telescope_timer_period;  end
  
  % check if window is already opened
  h = self.figure_handle;
  
  % raise interface if already there
  % set timer period
  if ~isempty(h)
    try
      % set(0,'CurrentFigure', h); % make it active
      figure(h);
      
      % restart the timer is not active
      if isa(self.telescope_timer,'timer') && isvalid(self.telescope_timer) 
        if strcmp(self.telescope_timer.Running, 'off') && dt > 0
          start(self.telescope_timer);
        end
        if dt > 0
          set(self.telescope_timer, 'Period', dt);
        end
      end
    catch
      h = [];
    end
  end
  
  % build interface when not there yet
  if isempty(h)
    % create
    if self.Verbosity > 0
      disp([ class(self) ': building interface. Please wait.' ]);
    end
    h = openfig('Telescope.fig');
    self.figure_handle = h;
    set(h, 'CloseRequestFcn',@inputdlg_CloseFigure);
    set(h, 'UserData', self, 'NumberTitle','off');
    
    % uicontrols
    Callbacks = { ...
    'telescope_status' @(src,evnt)getstatus(self,'full'); ...
    'telescope_s'      @(src,evnt)slew(self, 's'); ...
    'telescope_n'      @(src,evnt)slew(self, 'n'); ...
    'telescope_e'      @(src,evnt)slew(self, 'e'); ...
    'telescope_w'      @(src,evnt)slew(self, 'w'); ...
    'telescope_stop'   @(src,evnt)stop(self); ...
    'telescope_speed'  @(src,evnt)speed(self, [], num2str(get(gcbo, 'Value'))); ...
    'telescope_ra'     @(src,evnt)goto(self, get(gcbo, 'String'), get(findall(gcbf, 'Tag','telescope_dec'),'String')); ...
    'telescope_dec'    @(src,evnt)goto(self, get(findall(gcbf, 'Tag','telescope_ra'),'String'), get(gcbo, 'String')); ...
    'telescope_show_altaz', @inputdlg_SwitchAltAz_RADec; ...
    };
    
    inputdlg_build_callbacks(self, h, Callbacks);
    
    % set speed levels
    obj = findobj(h, 'Tag','telescope_speed');
    set(obj, 'Max', numel(self.Rate.Slew_Levels));
    
    % menus
    Callbacks = { ...
      'navigate_update',        @(src,evnt)getstatus(self,'full'); ...
      'navigate_stop',          @(src,evnt)stop(self,'hard'); ...
      'navigate_goto',          @(src,evnt)goto(self, findobj(self));
      'navigate_sync',          @(src,evnt)sync(self); ...
      'navigate_track_none',    @(src,evnt)track(self,'none'); ...
      'navigate_track_sidereal',@(src,evnt)track(self,'sidereal'); ...
      'navigate_home_goto',     @(src,evnt)home(self); ...
      'navigate_home_set',      @(src,evnt)home(self,'set'); ...
      'navigate_zoom_max',      @(src,evnt)speed(self,'max'); ...
      'navigate_zoom_find',     @(src,evnt)speed(self,'find'); ...
      'navigate_zoom_center',   @(src,evnt)speed(self,'center'); ...
      'navigate_zoom_guide',    @(src,evnt)speed(self,'guide'); ...
      'navigate_zoom_out',      @(src,evnt)speed(self,'increase'); ...
      'navigate_zoom_in',       @(src,evnt)speed(self,'decrease'); ...
      'view_findobj',           @(src,evnt)findobj(self); ...
      'view_skymap',            @(src,evnt)web(self); ...
      'view_plot',              @(src,evnt)plot(self); ...
      'view_plot_sky',          @(src,evnt)plot(self,'fov',360,'figure',figure); ...
    };
    
    inputdlg_build_callbacks(self, h, Callbacks);
    
    % set info text
    obj = findobj(h, 'Tag','telescope_info');
    set(obj,'String', [ class(self) ' ' self.Device_Port ]);
    
    % set RA/Dec and Status strings
    objra     = findobj(h, 'Tag','telescope_ra');
    objdec    = findobj(h, 'Tag','telescope_dec');
    set(objra , 'String', angle2hms(self, self.Coord.Equatorial.ra,  'hms'), ...
            'ToolTipString',sprintf('Right Ascension: %f [h]', self.Coord.Equatorial.ra));
    set(objdec,  'String', angle2hms(self, self.Coord.Equatorial.dec, 'dms'), ...
            'ToolTipString',sprintf('Declination: %f [deg]', self.Coord.Equatorial.dec));
    obj = findobj(h, 'Tag','telescope_status');
    set(obj, 'String', self.status);

  end
  
end
    
% ------------------------------------------------------------------------------

function out = inputdlg_build_callbacks(self, h, Callbacks)
  out = {};
  for index=1:size(Callbacks,1)
    obj = findobj(h, 'Tag', Callbacks{index,1});
    if ~isscalar(obj)
      if self.Verbosity > 0
        disp([ class(self) ': Invalid Tag ' Callbacks{index,1} ' in GUI' ])
      end
    elseif ~isempty(Callbacks{index,2})
      if any(strcmp(get(obj,'Type'),{'uipushtool','uitoggletool'}))
        set(obj, 'ClickedCallback', Callbacks{index,2})
      else
        set(obj, 'Callback', Callbacks{index,2})
      end
    else set(obj, 'Enable','off');
    end
    out{end+1}=obj;
  end
end
  
% ------------------------------------------------------------------------------
% Callbacks / timer
% ------------------------------------------------------------------------------

function inputdlg_CloseFigure(src,evnt)
% inputdlg_CloseFigure Close the Telescope control pad.

  h    = gcbf;
  self = get(h, 'UserData'); % figure.UserData
  
  if isvalid(self)
    self.figure_handle=[];
  end
  delete(h);
  
end % inputdlg_CloseFigure

% ------------------------------------------------------------------------------

function inputdlg_SwitchAltAz_RADec(src,evnt)
  % inputdlg_SwitchAltAz_RADec switch RA-Dec / Az-Alt display
  h    = gcbf;
  self = get(h, 'UserData'); % figure.UserData

  if isvalid(self)
    
    obj = findobj(h, 'Tag','telescope_show_altaz');
    if ~get(obj, 'Value') % show RA/DEC
      Callbacks = { ...
        'telescope_ra'     @(src,evnt)goto(self, get(gcbo, 'String'), get(findall(gcbf, 'Tag','telescope_dec'),'String')); ...
        'telescope_dec'    @(src,evnt)goto(self, get(findall(gcbf, 'Tag','telescope_ra'),'String'), get(gcbo, 'String')); ...
      };
      inputdlg_build_callbacks(self, h, Callbacks);
      obj = findobj(h, 'Tag','telescope_ra_moving');
      set(obj, 'String', 'RA');
      obj = findobj(h, 'Tag','telescope_dec_moving');
      set(obj, 'String', 'DEC');
    else % show Az/Alt
      Callbacks = { ...
        'telescope_ra'     @(src,evnt)goto(self, 'az',get(gcbo, 'String'), 'alt', get(findall(gcbf, 'Tag','telescope_dec'),'String')); ...
        'telescope_dec'    @(src,evnt)goto(self, 'az', get(findall(gcbf, 'Tag','telescope_ra'),'String'), 'alt', get(gcbo, 'String')); ...
      };
      inputdlg_build_callbacks(self, h, Callbacks);
      obj = findobj(h, 'Tag','telescope_ra_moving');
      set(obj, 'String', 'AZ');
      obj = findobj(h, 'Tag','telescope_dec_moving');
      set(obj, 'String', 'ALT');
    end
    
  end % isvalid
  
end % inputdlg_SwitchAltAz_RADec


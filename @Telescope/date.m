function [UTC, JD, lst, gmst] = date(self, varargin)
% DATE Compute local UTC date/time, including the time offset.
%
%   DATE(S) Computes UTC date/time from the current clock.
%
%   DATE(S, TIME) Sets UTC and LST date/times from the given time.
%   The UTC offset is then applied, except when time is given as a Time.UTC struct.
%
%   DATE(S, ..., Mount_Coord) Uses the given longitude/latitude settings (as a
%   struct with 'long','lat' fields).
%
%   [UTC, JD, LST, GMST] = DATE(S, ...) Returns the UTC ([6 values]), the Julian Day 
%   the Local Sidereal Time and the Greenwich Mean Sidereal Time (both in hours).
%   Only the Local Sidereal Time depends on location (longitude).
%
%   [...] = 

  UTC=[]; JD=[]; lst=[]; gmst=[];
  time  = [];
  Coord = self.Coord.Geographic;
  offset= self.Time.offset; % default offset
  
  % handle input arguments
  index=1;
  while index<=numel(varargin)
    s=varargin{index};
    if isstruct(s) && isfield(s, 'UTC')
      if any(strcmp(s.UTC, {'','auto','now','clock','iso8601'}))
        if isfield(time, 'offset'), offset= s.offset; end
        time  = clock;
      else
        time  = s.UTC;
        offset= 0;
      end
    elseif isstruct(s) && (isfield(s, 'long') && isfield(s, 'lat'))
      Coord = s;
    elseif ischar(s)
      switch s
      case {'equatorial','azimuthal'}
        Coord = self.Coord.Geographic;
      case 'time'
        if index < numel(varargin), time=varargin{index+1}; index=index+1; end
      otherwise
        time = s; % time as char
      end
    elseif isnumeric(s) && isempty(time) && any(numel(s) == [1 6])
      time = s;
    end
    index=index+1;
  end

  if isempty(time), time=clock; end
  if ischar(time) || (isnumeric(time) && isscalar(time))
    [year month day hour min sec] = datevec(time);
  elseif isnumeric(time) && numel(time) == 6
    time=num2cell(time);
    [year month day hour min sec] = deal(time{:});
  else
    error('UTC_time (2nd arg) should be given as char or 6-numbers vector or scalar or UTC-struct.');
  end
  
  if offset
    hour = hour - offset;
  end

  % compute Julian Date
  UT = hour + min/60 + sec/3600;
  J0 = 367*year - floor(7/4*(year + floor((month+9)/12))) ...
      + floor(275*month/9) + day + 1721013.5;
  JD = J0 + UT/24;              % Julian Day
  
  gmst = greenwichMeanSiderealTime(JD)*12/pi; % in rad->hour
  UTC  = [year month day hour min sec];
  lst  = rem(gmst+Coord.long/15, 24); % in hour, f(longitude)
  
  self.Time.datevec = UTC;
  self.Time.datestr = datestr(UTC); % iso8601
  if ~any(strcmp(self.Time.UTC, {'','auto','now','clock','iso8601'}))
    self.Time.UTC   = self.Time.datestr; % iso8601
  end
  self.Time.LST    = lst;
  self.Time.Julian = JD;

end

% ------------------------------------------------------------------------------

% Greg Miller (gmiller@gregmiller.net) 2021
% Released as public domain
% http://www.celestialprogramming.com/
% from: https://astrogreg.com/convert_ra_dec_to_alt_az.html

function gmst = greenwichMeanSiderealTime(jd)
    % "Expressions for IAU 2000 precession quantities" N. Capitaine1,P.T.Wallace2, and J. Chapront
    t = ((jd - 2451545.0)) / 36525.0;

    gmst=earthRotationAngle(jd)+(0.014506 + 4612.156534*t + 1.3915817*t*t - 0.00000044 *t*t*t - 0.000029956*t*t*t*t - 0.0000000368*t*t*t*t*t)/60.0/60.0*pi/180.0;  % eq 42
    gmst= rem(gmst,2*pi);
    if(gmst<0), gmst =gmst + 2*pi; end

end

function theta = earthRotationAngle(jd)
    % IERS Technical Note No. 32

    t = jd- 2451545.0;
    f = rem(jd, 1.0);

    theta = 2*pi * (f + 0.7790572732640 + 0.00273781191135448 * t); % eq 14
    theta = rem(theta, 2*pi);
    if theta<0, theta = theta+2*pi; end
end

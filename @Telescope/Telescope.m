classdef Telescope < handle
% TELESCOPE A base class to describe a telescope, inspired from INDIlib.
%   This class provides standard properties and methods.
%   Specific telescope drivers should inherit from Telescope and provide e.g.
%   the communication protocol, as well as all base operations.
%
% see http://docs.indilib.org/drivers/
% see https://www.indilib.org/api/classINDI_1_1Telescope.html
%
% To use this class, you should mostly derive a subclass from it, and redefine
% the following methods:
%
% - subclass constructor
% - align(self)
% - getstatus(self, {'full'})
% - gotoaxis(self, 'ax1',AX1,'ax2',AX2)
% - slewaxis(self, 'ax1',AX1,'ax2',AX2)
% - start(self)
% - stop(self)
% - speed(self, {AX}, degreesPerSecond)
% - track(self, degreesPerSecond)
%
% You may also set self.Camera=camera object with 'ishold' and 'image' methods.
%
% (c) E. Farhi 2024 - GPL3 

  % ------------------------------------------------------------------------------
  properties
    Camera          =[];
    Callback        =struct('onAnnotated',[],'onCaptured',[], ...
      'onGotoReached',[],'onSlewToReached',[],'onUpdate',[], ...
      'AutoAnnotate',true,'AutoAlign',true);
    Coord           =struct(); % all Coordinates
    Time            =struct('UTC', [], 'offset', 0); % Time UTC and offset in +hours/E (day-light)
    UserData        =[];  % for the User...
    Verbosity       = 1;  % 0=silent; 1=normal; 2=debug
  end
  
  % read-only properties
  properties(SetAccess=protected)
    Annotations     =[];    % array of struct with all image annotations
    Images          =[];    % array of struct with all image acquisitions
    Device_Port     ='';    % e.g. IP:PORT or SERIAL_DEV
    Rate            = struct('Slew',[],'Track',[],'Slew_Levels',logspace(0,3,8)*0.0042); % default slew rate speeds);     % Slew/Motion speed deg/s set with `speed`, Slew_Guide, Slew_Centering, Slew_Find, Slew_Max (Zoom level)
      % Tracking speed (string) set with `speed`, Track_Sidereal Track_Solar Track_Lunar Track_None
    status          ='INIT';  % the mount status, e.g. IDLE,TRACKING,SLEWING,GOTO
  end
  
  properties(Access=protected)
    Active_Devices  ='ACTIVE_TELESCOPE';
    figure_handle
    Capabilities    = struct(...
                      'TELESCOPE_CAN_ABORT',        true, ...
                      'TELESCOPE_CAN_CONTROL_TRACK',true, ...
                      'TELESCOPE_CAN_GOTO',         true, ...
                      'TELESCOPE_CAN_SLEWTO',       false, ...
                      'TELESCOPE_CAN_SYNC',         true, ...
                      'TELESCOPE_HAS_LOCATION',     true, ...
                      'TELESCOPE_HAS_TIME',         true, ...
                      'TELESCOPE_HAS_TRACK_MODE',   true, ...
                      'TELESCOPE_HAS_TRACK_RATE',   true); % INDIlib compat
                      
    catalogs        =[]; % Loaded at start, updated on findobj/plot
    Eq2Mount_T      =[];
    Annotations_Max = 100;  % max number of annotations to keep on annotate(s, 'clean')
    uid             =[];
    private              % some hidden stuff
    Slew_Target     =[]; % Target set with `slew`, e.g. struct('ax1','ax2','active',0|1)
    telescope_timer
    telescope_timer_period = 2; % update period, in seconds.
  end
  
  events % from https://www.indilib.org/api/classINDI_1_1Telescope.html
    GOTO_START        % when a goto is requested,       also triggers MOTION_START
    GOTO_STOP         % when a goto reaches its target, also triggers MOTION_STOP
    MOTION_START      % when a motion is initiated
    MOTION_STOP       % when a motion ends
    ANNOTATION_START  % when an annotation/solve-plate is requested
    ANNOTATION_STOP   % when an annotation ends
    CAPTURE_START     % when a capture is initiated (camera)
    CAPTURE_STOP      % when a capture ends
    UPDATED           % when a mount status update is done (e.g. periodic)
  end

  methods
  
    % SIMULATOR mode -----------------------------------------------------------
  
    % these methods should be overloaded by specific classes
    % In the following implementation, they emulate a fake equatorial mount.
    
    function s=Telescope(varargin)
    % TELESCOPE This is the base Telescope class. Default is a simulator.
    %
    %   S=TELESCOPE Initializes a simulated mount.
    
      if isempty(s.Device_Port), s.Device_Port    = 'simulator'; end
      [~,s.uid] = fileparts(tempname);  % set unique ID
      
      private_start(s); % starts main Timer(event) and sets all Coord defaults
      date(s);
      
      if isempty(s.catalogs) % load catalogs
        s.catalogs=getcatalogs(s);
      end
      % we initiate the Home coordinates (start)
      [s.Coord.Equatorial.ra, s.Coord.Equatorial.dec] = ...
        azalt2radec(s, s.Coord.Home.ax1, s.Coord.Home.ax2);
    end % Telescope (simulator)
    
    function align(self, varargin)
    % ALIGN Assigns position to axis (offset) useful to align/sync scope.
    %
    %   align(S) Assumes target is reached, sets all offsets accordingly.
    
    end % align (simulator)
    
    function s=getstatus(self, arg)
    % GETSTATUS Get the scope status, e.g. coordinates.
    %
    %   ST=GETSTATUS(S) Gets the current position and return the mount state
    %   which can be GOTO, SLEWING (when moving), or TRACKING/IDLE.
    %   In practice, the mount coordinates RA,Dec,Az,Alt,Ax1, Ax2 are 
    %   updated, as well as the slew speed level.
    %
    %   ST=GETSTATUS(S, 'full') Gets full mount state, as done when starting.
    
    % NOTE: the self.status is set from the Telescope_TimerCallback.
    
      persistent last_update;
      
      s = self.status;
      
      if ~isempty(last_update) && etime(clock, last_update) < 1
        return; 
      end

      % handle fake goto at each call (simulation)
      if isfield(self.Coord.Target,'active') && self.Coord.Target.active
        % the goto is done directly on RA/Dec as if the mount would be equatorial.
        
        d_ra = self.Coord.Target.ra - self.Coord.Equatorial.ra;
        d_dec= self.Coord.Target.dec- self.Coord.Equatorial.dec;
        if abs(d_ra)  < self.Rate.Slew/15
          self.Coord.Equatorial.ra = self.Coord.Target.ra;
        else
          self.Coord.Equatorial.ra = self.Coord.Equatorial.ra ...
                                   + sign(d_ra)*self.Rate.Slew/15;
        end
        if abs(d_dec) < self.Rate.Slew
          self.Coord.Equatorial.dec = self.Coord.Target.dec;
        else
          self.Coord.Equatorial.dec = self.Coord.Equatorial.dec ...
                                    + sign(d_dec)*self.Rate.Slew;
        end
        
        % get the 'real' mount axes
        [self.Coord.Horizontal.ax1, self.Coord.Horizontal.ax2] = ...
          radec2azalt(self, self.Coord.Equatorial.ra, self.Coord.Equatorial.dec);

      % handle fake tracking/moving at each call (simulation)
      elseif isfield(self.Slew_Target,'ax1') || isfield(self.Slew_Target,'ax2')
        % the slew is always done on Ax1/Ax2.
        
        % get the 'real' mount axes
        [self.Coord.Horizontal.ax1, self.Coord.Horizontal.ax2] = ...
          radec2azalt(self, self.Coord.Equatorial.ra, self.Coord.Equatorial.dec);

        if isfield(self.Slew_Target,'ax1')
          if     self.Coord.Horizontal.ax1 < self.Slew_Target.ax1
            % 'e','east','motion_east'
            self.Coord.Horizontal.ax1 = self.Coord.Horizontal.ax1 + self.Rate.Slew;
          elseif self.Coord.Horizontal.ax1 > self.Slew_Target.ax1
            % 'w','west','motion_west'
            self.Coord.Horizontal.ax1 = self.Coord.Horizontal.ax1 - self.Rate.Slew;
          end
        end
        if isfield(self.Slew_Target,'ax2')
          if     self.Coord.Horizontal.ax2 < self.Slew_Target.ax2
            % 'n','north','motion_north'
            self.Coord.Horizontal.ax2 = self.Coord.Horizontal.ax2 + self.Rate.Slew;
          elseif self.Coord.Horizontal.ax2 > self.Slew_Target.ax2
            % 's','south','motion_south'
            self.Coord.Horizontal.ax2 = self.Coord.Horizontal.ax2 - self.Rate.Slew;
          end
        end
        
        % Update RA/Dec
        [self.Coord.Equatorial.ra, self.Coord.Equatorial.dec] = ...
            azalt2radec(self, self.Coord.Horizontal.ax1, self.Coord.Horizontal.ax2);
      
      end
      
      % get Az/Alt coordinates
      [self.Coord.Horizontal.az, self.Coord.Horizontal.alt] = ...
        radec2azalt(self, self.Coord.Equatorial.ra, self.Coord.Equatorial.dec);

      last_update=clock;
      
    end % getstatus (simulator)
    
    function gotoaxis(self, varargin)
    % GOTOAXIS Send mount to given targets as a GOTO command.
    %
    %   GOTOAXIS(S, 'ax',target, ...) Sends axis 'ax' to target (degrees).
    %   The AX reference corresponds with a physical mount axis.
    
    % For a real mount, we should drive the mount axes 'ax1','ax2'.
    
      % we just activate the simulation flags: not a SLEW
      self.Slew_Target       = [];
      
      % get fastest route
      if     self.Coord.Target.ra  - self.Coord.Equatorial.ra  >  12 , self.Coord.Target.ra=self.Coord.Target.ra-24;
      elseif self.Coord.Target.ra  - self.Coord.Equatorial.ra  < -12 , self.Coord.Target.ra=self.Coord.Target.ra+24; end
      if     self.Coord.Target.dec - self.Coord.Equatorial.dec >  180, self.Coord.Target.dec=self.Coord.Target.dec-360;
      elseif self.Coord.Target.dec - self.Coord.Equatorial.dec < -180, self.Coord.Target.dec=self.Coord.Target.dec+360; end
      
      if isfield(self.Coord.Target,'az') && ~isempty(self.Coord.Target.az)
      if     self.Coord.Target.az  - self.Coord.Horizontal.az  >  180, self.Coord.Target.az=self.Coord.Target.az-360;
      elseif self.Coord.Target.az  - self.Coord.Horizontal.az  < -180, self.Coord.Target.az=self.Coord.Target.az+360; end
      end
      
      if isfield(self.Coord.Target,'alt') && ~isempty(self.Coord.Target.alt)
      if     self.Coord.Target.alt - self.Coord.Horizontal.alt >  180, self.Coord.Target.alt=self.Coord.Target.alt-360;
      elseif self.Coord.Target.alt - self.Coord.Horizontal.alt < -180, self.Coord.Target.alt=self.Coord.Target.alt+360; end
      end
      
      if isfield(self.Coord.Target,'ax1') && ~isempty(self.Coord.Target.ax1)
      if     self.Coord.Target.ax1 - self.Coord.Horizontal.ax1 >  180, self.Coord.Target.ax1=self.Coord.Target.ax1-360;
      elseif self.Coord.Target.ax1 - self.Coord.Horizontal.ax1 < -180, self.Coord.Target.ax1=self.Coord.Target.ax1+360; end
      end
      
      if isfield(self.Coord.Target,'ax2') && ~isempty(self.Coord.Target.ax2)
      if     self.Coord.Target.ax2 - self.Coord.Horizontal.ax2 >  180, self.Coord.Target.ax2=self.Coord.Target.ax2-360;
      elseif self.Coord.Target.ax2 - self.Coord.Horizontal.ax2 < -180, self.Coord.Target.ax2=self.Coord.Target.ax2+360; end
      end
      
    end % gotoaxis (simulator)
    
    function slewaxis(self, varargin)
    % SLEWAXIS Move axes to given targets, without affecting the goto target.
    %
    %   SLEWAXIS(S, 'ax') Moves axis with current speed. Axis reference
    %   can be 'n','s','e','w'. This corresponds with keypad actions.
    %
    %   SLEWAXIS(S, 'ax',target, ...) Sends axis 'ax' to target (degrees).
    %   The AX reference corresponds with a physical mount axis. The GOTO target
    %   is not affected. This corresponds with keypad actions towards given
    %   coordinates.
    %   To get a continuous move without target, set it target to +Inf/-Inf.
    
    % For a real mount, we should drive the mount axes 'ax1','ax2'.
    
      % we just activate the simulation flags: is a SLEW
      self.Slew_Target.active = true;
      
      % get fastest route
      if     self.Slew_Target.ra  - self.Coord.Equatorial.ra  >  12 , self.Slew_Target.ra=self.Slew_Target.ra-24;
      elseif self.Slew_Target.ra  - self.Coord.Equatorial.ra  < -12 , self.Slew_Target.ra=self.Slew_Target.ra+24; end
      if     self.Slew_Target.dec - self.Coord.Equatorial.dec >  180, self.Slew_Target.dec=self.Slew_Target.dec-360;
      elseif self.Slew_Target.dec - self.Coord.Equatorial.dec < -180, self.Slew_Target.dec=self.Slew_Target.dec+360; end
      
      if isfield(self.Slew_Target,'az') && ~isempty(self.Slew_Target.az)
      if     self.Slew_Target.az  - self.Coord.Horizontal.az  >  180, self.Slew_Target.az=self.Slew_Target.az-360;
      elseif self.Slew_Target.az  - self.Coord.Horizontal.az  < -180, self.Slew_Target.az=self.Slew_Target.az+360; end
      end
      
      if isfield(self.Slew_Target,'alt') && ~isempty(self.Slew_Target.alt)
      if     self.Slew_Target.alt - self.Coord.Horizontal.alt >  180, self.Slew_Target.alt=self.Slew_Target.alt-360;
      elseif self.Slew_Target.alt - self.Coord.Horizontal.alt < -180, self.Slew_Target.alt=self.Slew_Target.alt+360; end
      end
      
      if isfield(self.Slew_Target,'ax1') && ~isempty(self.Slew_Target.ax1)
      if     self.Slew_Target.ax1 - self.Coord.Horizontal.ax1 >  180, self.Slew_Target.ax1=self.Slew_Target.ax1-360;
      elseif self.Slew_Target.ax1 - self.Coord.Horizontal.ax1 < -180, self.Slew_Target.ax1=self.Slew_Target.ax1+360; end
      end
      
      if isfield(self.Slew_Target,'ax2') && ~isempty(self.Slew_Target.ax2)
      if     self.Slew_Target.ax2 - self.Coord.Horizontal.ax2 >  180, self.Slew_Target.ax2=self.Slew_Target.ax2-360;
      elseif self.Slew_Target.ax2 - self.Coord.Horizontal.ax2 < -180, self.Slew_Target.ax2=self.Slew_Target.ax2+360; end
      end
      
    end % slewaxis (simulator)
    
    function start(self, varargin)
    % START Initialize, and get full mount settings.
    
    % For a real mount, we should initialize the mount in 'startup'state.
    
      private_start(self, varargin{:}); % (..., dt)
    end % start (simulator)

    function degreesPerSecond=speed(self, ax, degreesPerSecond)
    % SPEED Set the slew speed in degrees per second.
    %
    %   SPEED(S, deg_per_sec) Sets all mount axes speed to degrees per second.
    %   where deg_per_sec is in deg/s, 'sidereal','center','find','max'
    %   or '1'-'n' (level given as a string), or 'stop'.
    %
    %   SPEED(S, AX, deg_per_sec) Sets given axis speed. Use 'stop' or 0 to stop.
    %
    %   SPEED(S) Retuns the current speed in deg per seconds.
    
      if nargin  == 1,
        degreesPerSecond = self.Rate.Slew;
        return
      elseif nargin == 2
        degreesPerSecond = ax; ax = [];
      end
      
      degreesPerSecond = speedalias(self, degreesPerSecond);
      if isempty(degreesPerSecond), return; end
      if any(strcmp(ax, {'ax1','ax2'})) && isfield(self.Slew_Target, ax) ...
          && degreesPerSecond==0
        self.Slew_Target = rmfield(self.Slew_Target, ax);
      elseif ~isempty(degreesPerSecond)
        self.Rate.Slew = degreesPerSecond;
      end
      
    end % speed (simulator)
    
    function stop(self, varargin) % stop current move
    % STOP Stop the mount move.
    
      self.Slew_Target           = [];      % stop running slew/move
      dps=self.Rate.Slew;
      speed(self, 'stop');
      speed(self, 'ax1', 0);
      speed(self, 'ax2', 0);
      self.Coord.Target.active  = false;    % stop goto
      speed(self, dps); % restore previous speed state after stopping
    end % stop (simulator)
    
    function degreesPerSecond=track(self, degreesPerSecond)
    % TRACK Set the tracking speed in degrees per second, usually sidereal.
    %
    %   TRACK(S, 'sidereal') Sets  mount tracking speed to sidereal.
    %   TRACK(S, 'none')     Stops mount tracking.
    %
    %   TRACK(S, deg_per_sec) Sets given axis tracking speed, where 
    %   deg_per_sec in in deg/s or 'sidereal' or 'none'.
    %
    %   TRACK(S) Retuns the current tracking speed in deg per seconds.
    
      if nargin  == 1,
        degreesPerSecond = self.Rate.Track;
        return
      end
      
      speedalias(self, degreesPerSecond); % sets Rate.Track
      
    end % track (simulator)
    
  end % public methods
  
end % classdef


function d_alt = alt_refraction(alt, T, P)
% ALT_REFRACTION Compute the altitude/elevation corrected with refraction.
%
%   Delta_Alt = ALT_REFRACTION(alt) Computes the refraction correction to be added
%   to the actual altitude/elevation (given in deg, 90 at Zenith, 0 is horizon).
%
%   Delta_Alt = ALT_REFRACTION(alt, T, P) As above with given temperature T in Celsius
%   and pressure P in mB.
%
% Reference: Astronomical Algorithms by Jean Meeus, page 102.
%   https://britastro.org/2019/atmospheric-refraction


  if nargin < 2, T=15;   end  % temperature in C
  if nargin < 3, P=1013; end  % pressure    in mB
  
  R     = 1.02./tand(alt+(10.3./(alt+5.11)));
  S     = (R*P/1010*283/(273+T));
  d_alt = S/60; % arcmin -> deg
  
end

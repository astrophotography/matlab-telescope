function [fig, object_list] = annotate_plot(self, ret, fig)
% ANNOTATE_PLOT Plot an annotation from e.g. self.Annotations

  handles = []; object_list = [];
  if nargin < 3, fig=[]; end
  if isempty(ret), return; end
  if isfield(ret, 'results')
    ret=ret.results;
  end
  
  if ~strcmp(ret.status, 'success') || ~isfieldi(ret, 'RA')
    return
  end
  
  % compute FontSize from DPI, for labels.
  dpi      = max(get(0,'screensize'))/13; % 13" screen
  FontSize = max(get(0,'DefaultUicontrolFontSize'), 12*dpi/72);
  
  im  = imread(ret.filename);
  if isempty(fig)
    fig = figure('Name', [ mfilename ': ' ret.filename ]);
  else
    if strcmp(get(fig,'type'),'axes')
      axes(fig);
      fig = get(fig,'Parent');
    end
    set(0, 'CurrentFigure', fig);
  end
  imh    = image(im); sz=size(im); sz=sz([2 1]); sz=sz/max(sz);
  set(gca, 'xtick',[],'ytick',[], 'XTickLabel', {}, 'YTickLabel', {});
  set(gca, 'Color',    'k'); % black background
  
  [p,f,e] = fileparts(ret.filename);
  title([ f e ' in ' ret.constellation ], 'FontSize', FontSize);
  hold on
  
  % central coordinates
  sz = ret.size/2;
  ht = plot(sz(1), sz(2), 'r+'); set(ht, 'MarkerSize', 16);

  object_list = ret.object_list;
  for v=object_list % catalog
  
    catalog = v{1};
    % plot each 'catalog' category with given 'symbol'
    handles = [ handles ...
      plot_xy(catalog.catalog, catalog.X, catalog.Y, catalog.MAG,  catalog.TYPE, ...
                        catalog.SIZE) ];
    
    %  tag the 'n' brightest objects for each category
    n = 10;
    if numel(catalog.X) > n
      index=1:n;
      catalog = sort_catalog(catalog, index);
    end
    w = warning('off');
    for index=1:numel(catalog.NAME)
      ht = [ ht ...
        text(catalog.X(index),catalog.Y(index), catalog.NAME{index},  ...
          'Color','y', 'FontSize', FontSize) ];
    end
    warning(w);
    
  end
  
  if isfield(ret,'object_str')
    set(gca, 'UserData', sprintf('%s\n', ret.object_str{:}));
  end
  set(ht,  'Tag', 'Telescope_Plot_Labels');
  pbaspect([sz  1]); % IMPORTANT: aspect ratio must be ~1.
  
  m = uicontextmenu; % ('Parent', fig);
  uimenu(m, 'Label', '<html><b>Field center</b></html>');
  uimenu(m, 'Label', [ 'Image: ' ret.filename ]);
  uimenu(m, 'Label', [ 'RA='  angle2hms(self, ret.RA, 'hms') ]);
  uimenu(m, 'Label', [ 'DEC=' angle2hms(self, ret.DEC,'dms') ]);
  uimenu(m, 'Label', [ 'Rotation= ' num2str(ret.rotation) ' [deg]' ]);
  uimenu(m, 'Label','Show labels', 'Callback','set(findall(gca, ''Tag'',''Telescope_Plot_Labels''),''visible'',''on'')', 'Separator','on');
  uimenu(m, 'Label','Hide labels', 'Callback','set(findall(gca, ''Tag'',''Telescope_Plot_Labels''),''visible'',''off'')');
  uimenu(m, 'Label','Copy Objects to clipboard', 'Callback', [ ...
    'clipboard(''copy'',get(gcba, ''UserData''));' ]);
  set(imh, 'UIContextMenu', m);
  
end % annotate_plot

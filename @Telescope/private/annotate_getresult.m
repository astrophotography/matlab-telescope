function [ret, descr] = annotate_getresult(self, d, filename)
  % ANNOTATE_GETRESULT: extract WCS and informations from the FITS output files.
  %
  % input:
  %   d:        directory where astrometry.net results are stored.
  %   filename: the initial image.
  
  ret = []; descr = '';
  
  ret.filename = filename;
  ret.dir      = d;
  ret.status   = 'failed';
  
  for file={'results.wcs','wcs.fits'}
    if exist(fullfile(d, file{1}), 'file')
      ret.wcs  = read_fits(fullfile(d, file{1}));
      
      % get image center and print it
      if isfield(ret.wcs,'meta') && isfield(ret.wcs.meta,'CRVAL1')
        wcs = ret.wcs.meta;
        ret.wcs.meta.CD = [ wcs.CD1_1 wcs.CD1_2 ; 
                            wcs.CD2_1 wcs.CD2_2 ];
                            
        % get central coordinates
        
        ret.size     = [ wcs.IMAGEW wcs.IMAGEH ];
        sz  = ret.size/2;

        [ret.RA, ret.DEC] = xy2sky_tan(ret.wcs.meta, sz(1), sz(2)); % MAAT Ofek (private)
        ret.RA=rem(ret.RA*180/pi/15,24); ret.DEC=ret.DEC*180/pi;

        % compute pixel scale
        ret.pixel_scale = sqrt(abs(wcs.CD1_1 * wcs.CD2_2  - wcs.CD1_2 * wcs.CD2_1))*3600; % in arcsec/pixel
        % compute rotation angle
        ret.rotation = atan2(wcs.CD2_1, wcs.CD1_1)*180/pi;
        
        % identify closest constellation we are in
        [m, index] = min( (ret.RA - self.catalogs.constellations.RA).^2 ...
                        + (ret.DEC- self.catalogs.constellations.DEC).^2 );
        ret.constellation = self.catalogs.constellations.NAME{index};
        ret.object_list = {};
        
        % identify objects in field
        for catalogs = {'stars','deep_sky_objects'}
          % find all objects from data base within bounds
          catalog = self.catalogs.(catalogs{1});
          [catalog.X, catalog.Y] = sky2xy_tan(ret.wcs.meta, ...
            catalog.RA*pi/180*15, catalog.DEC*pi/180);
          % ignore when not on image
          index   = find(1 <= catalog.X & catalog.X <= ret.size(1) ...
                       & 1 <= catalog.Y & catalog.Y <= ret.size(2) ...
                       & ret.DEC.*catalog.DEC > 0);
          if isempty(index), continue; end
          catalog = sort_catalog(catalog, index);
          ret.object_list{end+1} = catalog;
        end
        ret.status   = 'success';
        % store results as the last annotation coordinates
        self.Coord.Annotation.ra = ret.RA;
        self.Coord.Annotation.dec= ret.DEC;
        
        break
      end
    end
  end % for
  
  for file={'results.rdls','rdls.fits'}
    if exist(fullfile(d, file{1}), 'file')
      ret.rdls = read_fits(fullfile(d, file{1}));
    end
  end
  for file={'results.corr','corr.fits'}
    if exist(fullfile(d, file{1}), 'file')
      ret.corr = read_fits(fullfile(d, file{1}));
    end
  end
  if exist(fullfile(d, 'results.json'), 'file')
    ret.json = loadjson(fullfile(d, 'results.json'));
  end
  
  % rename annotated image (if any)
  dr = dir(fullfile(d, '*-ngc.png'));
  if numel(dr) == 1
    movefile(fullfile(d, dr.name), fullfile(d, 'annotated.png'));
  end
  
  % output reports
  if isfieldi(ret,'RA') && strcmp(ret.status, 'success')
    % markdown/text, and set 'cliboard' content (object_str)
    [ret.object_str, ret.description] = annotate_saveas(self, ret, 'md');
    % web page
    annotate_saveas(self, ret, 'html');
    descr = ret.description;
  end
  
end % annotate_getresult

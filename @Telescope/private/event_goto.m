function status = event_goto(self, status)
% EVENT_CAPTURE Event loop: Check when reaching target (onGotoReached).

  d_ra   = self.Coord.Target.ra - self.Coord.Equatorial.ra;
  d_dec  = self.Coord.Target.dec- self.Coord.Equatorial.dec;
  status = 'GOTO';

  % goto is close to target
  if abs(d_ra)*15  < self.Rate.Slew_Levels(1) && abs(d_dec) < self.Rate.Slew_Levels(1)
    status = [ status ' REACHED' ];
    notify(self, 'GOTO_STOP');  % MOTION_STOP below
    self.Coord.Target.active = false;
    event_evalfun(mfilename, self.Callback.onGotoReached, self);
    
    % the 'stop' command is included in goto commands from the mount firmware...
    if self.Verbosity > 0
      disp([ '[' datestr(now) '] ' class(self) ': GOTO REACHED: RA=' num2str(self.Coord.Target.ra) ' DEC=' num2str(self.Coord.Target.dec) ]);
    end
  end
    
end % event_goto

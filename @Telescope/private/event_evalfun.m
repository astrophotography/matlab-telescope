function event_evalfun(evt, fun, self, varargin)
% EVENT_EVALFUN evaluate the function callback after event
%   EVENT_EVALFUN(evt, fun, self, ...) Calls 'fun' as a event of type 'evt' (str).
%   The callback should have syntax fun(self, ...)

  if ~isempty(fun) && (isa(fun,'function_handle') || ischar(fun))
    try
      if self.Verbosity > 1
        if isa(fun,'function_handle'), f=func2str(fun); else f=fun; end
        disp([ '[' datestr(now) '] ' mfilename  ': evaluating ' evt ' ' f ]);
      end
      feval(fun, self, varargin{:});
    catch ME
      if self.Verbosity > 0
        if isa(fun,'function_handle'), fun=func2str(fun); end
        disp([ mfilename ': ERROR when evaluating ' evt ' ' fun ]);
        getReport(ME)
      end
    end
  end
end

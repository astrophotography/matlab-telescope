function [ra,dec,az,alt,ax1,ax2,name,lat,long,time] = parseparams(self, varargin)
% PARSEPARAMS Handle input arguments and isolate RA-Dec/Az-Alt/AX/Lat-Long.
%
%   [ra,dec,az,alt,ax1,ax2,name,lat,long,time] = parseparams(self, ...)

  ra=[]; dec=[]; alt=[]; az=[]; ax1=[]; ax2=[]; 
  lat=[]; long=[]; name=[]; time=[];
  
  % handle input arguments
  if nargin ==1, return;
  elseif nargin == 2 && ischar(varargin{1})
    switch varargin{1}
    case 'n'
      ax2=+Inf; 
    case 's'
      ax2=-Inf; 
    case 'e'
      ax1=+Inf; 
    case 'w'
      ax1=-Inf; 
    otherwise
      varargin{1} = findobj(self, varargin{1});
    end
  elseif nargin == 3 && isnumeric(varargin{1}) && isnumeric(varargin{2})
    ra  = varargin{1};
    dec = varargin{2};
  end
  if isstruct(varargin{1})
    c = varargin{1};
    [tf,f]=isfieldi(c, 'ra');  if tf, ra  =c.(f); end
    [tf,f]=isfieldi(c, 'dec'); if tf, dec =c.(f); end
    [tf,f]=isfieldi(c, 'alt'); if tf, alt =c.(f); end
    [tf,f]=isfieldi(c, 'az');  if tf, az  =c.(f); end
    [tf,f]=isfieldi(c, 'name');if tf, name=c.(f); end
    [tf,f]=isfieldi(c, 'lat');  if tf, lat =s.(f); end
    [tf,f]=isfieldi(c, 'long'); if tf, long=s.(f); end
    [tf,f]=isfieldi(c, 'time'); if tf, time=s.(f); end
  end
  % now search for char/numeric args and name/value pairs
  n = numel(varargin);
  i = 1; 
  while i<=n
    if     strcmpi(varargin{i}, 'ra' ) && i < n, ra =varargin{i+1}; i=i+1;
    elseif strcmpi(varargin{i}, 'dec') && i < n, dec=varargin{i+1}; i=i+1;
    elseif strcmpi(varargin{i}, 'az')  && i < n, az =varargin{i+1}; i=i+1;
    elseif strcmpi(varargin{i}, 'alt') && i < n, alt=varargin{i+1}; i=i+1;
    elseif strcmpi(varargin{i}, 'ax1') && i < n, ax1=varargin{i+1}; i=i+1;
    elseif strcmpi(varargin{i}, 'ax2') && i < n, ax2=varargin{i+1}; i=i+1;
    elseif strcmpi(varargin{i}, 'lat') && i < n, lat=varargin{i+1}; i=i+1;
    elseif strcmpi(varargin{i}, 'long') && i < n, long=varargin{i+1}; i=i+1;
    elseif strcmpi(varargin{i}, 'time') && i < n, time=varargin{i+1}; i=i+1;
    elseif strcmpi(varargin{i}, 'n'), ax2=+Inf; 
    elseif strcmpi(varargin{i}, 's'), ax2=-Inf; 
    elseif strcmpi(varargin{i}, 'e'), ax1=+Inf; 
    elseif strcmpi(varargin{i}, 'n'), ax1=-Inf;
    % or numeric in order: ra,dec,az,alt,time(6)
    elseif isnumeric(varargin{i})
      if isempty(time) && numel(varargin{i}) == 6
        time = varargin{i};
      elseif isempty(ra),  ra =varargin{i};
      elseif isempty(dec), dec=varargin{i};
      elseif isempty(az),  az =varargin{i};
      elseif isempty(alt), alt=varargin{i};
      elseif isempty(time),time=varargin{i};
      end
    % or char in order: ra,dec,time
    elseif ischar(varargin{i})
      if     isempty(ra),  ra =varargin{i};
      elseif isempty(dec), dec=varargin{i};
      elseif isempty(time),time=varargin{i};
      end
    end
    i=i+1;
  end

end

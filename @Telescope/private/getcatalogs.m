function catalogs = getcatalogs(self)
  % GETCATALOGS Load catalogs.
  % 
  % load catalogs: stars, deep sky objects
  %
  % The RA is given in hours, and Dec in degrees.
  
  % catalogs are better saved in -v7 format (much smaller files).
  
  persistent c
  
  catalogs = [];
  
  if isempty(c) % first call
    if self.Verbosity > 0
      disp([ '[' datestr(now) '] ' class(self) ' Loading Catalogs...' ]);
    end
    
    c = load(mfilename);
    
    % compute planets
    place = [ self.Coord.Geographic.long self.Coord.Geographic.lat ];
    c.planets = getcatalogs_planets(self.Time.Julian, place, self.Time.LST);
    
    % display available catalogs
    for f=fieldnames(c)'
      name = f{1};
      if ~isempty(c.(name)) && isfield(c.(name), 'RA')
        num  = numel(c.(name).RA);
        if isfield(c.(name), 'Description')
          desc = c.(name).Description;
        else desc = ''; end
        if self.Verbosity > 0
          disp([ 'Catalog: ' name ' with ' num2str(num) ' entries.' ]);
          disp([ '  ' desc ])
        end
        % fix object names
        if isfield(c.(name), 'NAME')
          c.(name).NAME = regexprep(c.(name).NAME, '\s*',' ');
        end
        c.(name).catalog = name;
      end
    end
  end
  catalogs = c;
  
end % getcatalogs

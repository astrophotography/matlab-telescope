function h = plot_xy(f, x,y, mag, typ, sz)
% PLOT_XY Plot a set of X,Y points with symbols/colours depending on the catalog 'f' 

  % we use scatter plots: only the 'circle' can have non-uniform size
  switch f
  case 'planets'
    % For planets, coloured disk (scatter) with size=Magnitude^2 colour 
    h = scatter(x,y, markersize(mag).^(2), 'r', 'filled');
    
  case 'stars'
    % For stars, use coloured (scatter) with size=Magnitude^2 colour from TYPE
    SZ       = 10-mag+min(mag); % from 10 down ...
    SZ(SZ<1) = 1;
    h = scatter(x,y, SZ.^2, colour(typ,mag), 'filled');
    
  case 'deep_sky_objects'
    % For DSO,   use circle (scatter) with thickness=Magnitude, and given size
    mag(isnan(mag) | mag == 0) = 15;
    SZ1 = markersize(mag); SZ2=zeros(size(sz));
    index = find(sz>0);
    SZ2(index) = sz(index)/60*1024;
    SZ = max(SZ1(:).^(2), SZ2(:).^2); SZ=max(SZ,1);
    h = scatter(x,y, SZ, colour(typ,mag), 'o'); % large areas
    
  otherwise
    h = scatter(x,y, markersize(mag), 'w');
  end

end % plot_xy

function c = colour(typ, mag)
  % COLOUR Determine the colour of objects for scatter3
  c = ones(numel(typ),3);  % initialise to white
  
  tokens = { 'star O',  [ 0 0   1 ]; ...
             'star B',  [ 0 0.5 1 ]; ...
             'star A',  [ 0 1   1 ]; ...
             'star F',  [ 0 1   0 ]; ...
             'star G',  [ 1 1   0 ]; ...
             'star K',  [ 1 0.5 0 ]; ...
             'star M',  [ 1 0   0 ]; ...
             'DSO C',   [ 1 0 0 ]; ...
             'DSO D',   [ 1 0 0 ]; ...
             'DSO E',   [ 1 0 0 ]; ...
             'DSO I',   [ 1 0 0 ]; ...
             'DSO P',   [ 1 0 0 ]; ...
             'DSO G',   [ 1 0 0 ]; ...
             'DSO S',   [ 1 0 0 ]; ...
             'DSO OCL', [ 0 0 1 ]; ...
             'DSO GCL', [ 0 1 1 ]; ...
             'DSO DN',  [ 0 1 0 ]; ...
             'DSO EN',  [ 0 1 0 ]; ...
             'DSO RN',  [ 0 1 0 ]; ...
             'DSO PN',  [ 0 1 0 ]; ...
             'DSO *',   [ 0 0 0 ]; ...
             'DSO NF',  [ 0 0 0 ]; ...
             'DSO OC',  [ 0 0 0 ];};
  mag1 = 1-(mag-min(mag))*.1;
  mag1(mag1 < .5 | mag==0) = .5;

  for index=1:size(tokens, 1)
    tok = tokens{index, 1};
    col = tokens{index, 2};
    ok  = strncmp(typ, tok, numel(tok));
    c(ok,1) = col(1).*mag1(ok); 
    c(ok,2) = col(2).*mag1(ok); 
    c(ok,3) = col(3).*mag1(ok);
  end
end % colour

function m = markersize(mag)
  % markersize(mag): compute the marker size from the magnitude
  
  m = log(13-mag)*3+1;
  m(mag>=13) = 1;
  m = ceil(abs(m));
end % markersize

function catalog = sort_catalog(catalog, index)
% SORT_CATALOG Select index elements from the catalog.
  catalog.RA   = catalog.RA(index);
  catalog.DEC  = catalog.DEC(index);
  catalog.MAG  = catalog.MAG(index);
  catalog.SIZE = catalog.SIZE(index);
  catalog.TYPE = catalog.TYPE(index);
  catalog.NAME = catalog.NAME(index);
  catalog.DIST = catalog.DIST(index);
  if isfield(catalog,'X'), catalog.X=catalog.X(index); end
  if isfield(catalog,'Y'), catalog.Y=catalog.Y(index); end
end

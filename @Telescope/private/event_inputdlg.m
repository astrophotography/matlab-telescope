function last_switch = event_inputdlg(self, last_switch, last_ra, last_dec, last_az, last_alt)
% EVENT_INPUTDLG Event loop: Update user interface.
 
  h = self.figure_handle;
  
  if ~isempty(h)
    try
      c = [ 'RA=' angle2hms(self, self.Coord.Equatorial.ra, 'hms') ...
            ' DEC=' angle2hms(self, self.Coord.Equatorial.dec,'dms') ...
            ' ' self.status ];
      set(h, 'Name', c);
    catch ME
      if self.Verbosity > 1
        disp([ '[' datestr(now) '] ' class(self) ': WARNING: Failed setting status.' ])
      end
    end
    
    % report status and values into GUI
    obj = findobj(h, 'Tag','telescope_status');
    set(obj, 'String', self.status);
    obj = findobj(h, 'Tag','telescope_info');
    if isfield(self.Time,'datevec')
      UTC = self.Time.datestr;
    elseif isfield(self.Time,'datevec')
      UTC = datestr(self.Time.datevec);
    else
      UTC = self.Time.UTC;
    end
    set(obj, 'ToolTipString', [ 'UTC: ' UTC ]);
    
    % get closest speed level
    [~,i] = min(abs(self.Rate.Slew - self.Rate.Slew_Levels));
    obj = findobj(h, 'Tag','telescope_speed');
    set(obj, 'Value', i); 
    
    % check if changed since last call, and light-on the LED if so.
    objswitch = findobj(h, 'Tag','telescope_show_altaz');
    
    
    if ~isempty(objswitch)
      objra     = findobj(h, 'Tag','telescope_ra');
      objdec    = findobj(h, 'Tag','telescope_dec');
      objra_mov = findobj(h, 'Tag','telescope_ra_moving');
      objdec_mov= findobj(h, 'Tag','telescope_dec_moving');
      set([objra_mov objdec_mov], 'Value' , 0);
      sw = get(objswitch, 'Value');
    
      if ~sw % show RA/DEC
        if last_ra ~= self.Coord.Equatorial.ra || last_switch ~= sw
          tt = sprintf('Right Ascension: %f [h]', self.Coord.Equatorial.ra);
          if     isfield(self.Slew_Target,'ra') 
            tt = [ tt sprintf('\nTarget: %f [h]', self.Slew_Target.ra) ];
          elseif isfield(self.Coord.Target,'ra') 
            tt = [ tt sprintf('\nTarget: %f [h]', self.Coord.Target.ra) ];
          end
          set(objra ,    'String', angle2hms(self, self.Coord.Equatorial.ra,  'hms'), ...
            'ToolTipString',tt);
          set(objra_mov, 'Value' , 1);
        end
        if last_dec ~= self.Coord.Equatorial.dec || last_switch ~= sw
          tt = sprintf('Declination: %f [deg]', self.Coord.Equatorial.dec);
          if     isfield(self.Slew_Target,'dec') 
            tt = [ tt sprintf('\nTarget: %f [deg]', self.Slew_Target.dec) ];
          elseif isfield(self.Coord.Target,'dec')
            tt = [ tt sprintf('\nTarget: %f [deg]', self.Coord.Target.dec) ];
          end
          set(objdec,     'String', angle2hms(self, self.Coord.Equatorial.dec, 'dms'), ...
            'ToolTipString',tt);
          set(objdec_mov, 'Value' , 1);
        end
        
      else                        % show Az-Alt
        if last_az ~= self.Coord.Horizontal.az || last_switch ~= sw
          tt = sprintf('Azimuth: %f [deg]', self.Coord.Horizontal.az);
          if     isfield(self.Slew_Target,'az') 
            tt = [ tt sprintf('\nTarget: %f [deg]', self.Slew_Target.az) ];
          elseif isfield(self.Coord.Target,'az')
            tt = [ tt sprintf('\nTarget: %f [deg]', self.Coord.Target.az) ];
          end
          set(objra , 'String', num2str(self.Coord.Horizontal.az ), ...
            'ToolTipString',tt);
          set(objra_mov, 'Value' , 1);
        end
        if last_alt ~= self.Coord.Horizontal.alt || last_switch ~= sw
          tt = sprintf('Altitude/Elevation: %f [deg]', self.Coord.Horizontal.alt);
          if     isfield(self.Slew_Target,'alt') 
            tt = [ tt sprintf('\nTarget: %f [deg]', self.Slew_Target.alt) ];
          elseif isfield(self.Coord.Target,'alt')
            tt = [ tt sprintf('\nTarget: %f [deg]', self.Coord.Target.alt) ];
          end
          set(objdec, 'String', num2str(self.Coord.Horizontal.alt), ...
            'ToolTipString',tt);
          set(objdec_mov, 'Value' , 1);
        end
      end
      last_switch = sw;
    end
  end % ~isempty(h)
 
end % event_inputdlg

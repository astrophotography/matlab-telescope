function private_start(self, dt)
% PRIVATE_START Initialize, and get full mount settings.
%
%   PRIVATE_START(S, DT) Sets the update timer period DT (seconds).

  if nargin < 2, dt=[]; 
  else self.telescope_timer_period = dt; end
  if isempty(dt), dt=self.telescope_timer_period; end
  
  self.Coord.Geographic=struct('lat', 48, 'long', 2); % GPS coordinates (East long, North lat)
  self.Coord.Equatorial=struct();                     % Current scope Eq coordinates, struct('hours',  'degrees')
  self.Coord.Home      =struct('ax1',-90,'ax2',0);    % The mount HOME, e.g. West Horizon
  self.Coord.Horizontal=struct();                     % Current scope Az coordinates, all degrees
  self.Coord.Target    =struct();                     % Target set with `goto`, struct('ra','dec','alt','az','name','active',0|1)
  self.Coord.Annotation=struct();                     % last annotation
  self.Coord.Equatorial.ra =0;  % hours
  self.Coord.Equatorial.dec=0;  % deg
  self.Coord.Horizontal.az =0;  % deg
  self.Coord.Horizontal.alt=0;  % deg
  self.Coord.Horizontal.ax1=0;  % deg
  self.Coord.Horizontal.ax2=0;  % deg
  
  if isa(self.telescope_timer,'timer') && isvalid(self.telescope_timer) 
    if strcmp(self.telescope_timer.Running, 'off') && dt > 0
      start(self.telescope_timer);
    end
    if dt > 0
      set(self.telescope_timer, 'Period', dt);
    end
  else
    % create the timer for auto update
    self.telescope_timer  = timer('TimerFcn', @(t,e)event(self, t), ...
        'Period', dt, 'ExecutionMode', 'fixedDelay', 'UserData', self, ...
        'Name', [ class(self) '-' self.uid ]);
  
    start(self.telescope_timer);
  end
  
  % call the timer
  try
    event(self, 'null');
  catch ME
    getReport(ME)
  end
  
  speed(self, 'find');
end % private_start

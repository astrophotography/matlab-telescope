function [s,f] = setfieldi(s, field, v)
% SETFIELDI Sets value of case insensitive field in structure.
%
%   [S, F] = SETFIELDI(S, field, value) Sets value of case insensitive
%   'field' and returns the modified structure and matching field F.

  tf = false; f=''; v=[];
  if ~isstruct(s), return; end
  
  fields = fieldnames(s);
  index = strcmpi(field, fields);
  if isscalar(index)
    f     = fields{index};
    s.(f) = v;
  end
end

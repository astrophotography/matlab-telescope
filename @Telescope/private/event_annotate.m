function ret=event_annotate(self)
% EVENT_ANNOTATE Scan all past annotations and read their status.
%   execute onAnnotated callbacks
%   returns the latest successful annotation.

  ret=[];
  if isempty(self.Annotations) || ~isstruct(self.Annotations), return; end

  running = find(strcmp({self.Annotations.status}, 'running'));

  for index=running
    this = self.Annotations(index);
    if strcmp(this.status, 'running') && isdir(this.dir) && isjava(this.process_java)
      % we check if the process is still running, and update its status
      try
        this.exitValue = this.process_java.exitValue; % will raise error if process still runs
        active    = 0;
      catch ME % still running
        active    = 1;
      end
      
      % not active anymore: process has ended.
      if ~active
        % get the output from the command
        this.stdout   = process_get_output(this.process_java.getOutputStream);
        this.stderr   = process_get_output(this.process_java.getErrorStream);
        this.time_stop = clock;
        c = [];
        if this.exitValue == 0
          [this.results, c] = annotate_getresult(self, this.dir, this.filename);
        end
        
        if ~isempty(this.results)
          if isfield(this.results, 'status')
            this.status   = this.results.status;
          else
            this.status   = 'success';
          end
        else
          this.status   = 'failed';
        end
        % display results
        if self.Verbosity > 0
          disp(char(c));
        end
        this.description = c;
        self.Annotations(index) = this;
        
        % evaluate callback on success
        if strcmp(this.status,'success') && isfield(this.results,'RA') && isfield(this.results,'DEC')
          % store annotation Coords
          self.Coord.Annotation.ra =this.results.RA;
          self.Coord.Annotation.dec=this.results.DEC;
          % copy back coordinates into image struct record
          if isfield(this, 'image') && isstruct(this.image) && isfield(this.image,'index')
            try
              self.Images(this.image.index).results = this.results;
            catch
              if self.Verbosity > 1
                disp([ '[' datestr(now) '] ' mfilename  ': WARNING: Could not update image data' ])
              end
            end
            % trigger autoalign if set. Must not move.
            if any(strncmp(self.status, {'SLEW','GOTO'}, 4))
              if this.autoalign && self.Verbosity > 0
                disp([ '[' datestr(now) '] ' mfilename  ': WARNING: Can not autoalign: ' self.status ])
              end
              this.autoalign = false;
            end
            if this.autoalign
              % compute shift. this.results.RA|DEC - self.Coord.Equatorial
              % we compute distance on the sphere, using cartesian coordinates
              [xm,ym,zm]   = sph2cart(self.Coord.Equatorial.ra*15*pi/180, self.Coord.Equatorial.dec*pi/180, 1); % mount
              [xi,yi,zi]   = sph2cart(this.results.RA*15*pi/180, this.results.DEC*pi/180, 1); % image
              delta = norm([xm,ym,zm] - [xi,yi,zi])*180/pi; % estimate angle in [deg]
              if self.Verbosity > 0
                disp([ '[' datestr(now) '] ' mfilename  ': AutoAlign on Annotate: Shift [deg] is ' num2str(delta) ])
              end
            end
          end % image present
          
          % notify/print
          if self.Verbosity > 1
            l = [ '<a href="matlab:web(Telescope,''' this.dir ''');">' this.dir '</a>' ];
            disp([ '[' datestr(now) '] ' class(self) ': ANNOTATION_STOP: RA=' num2str(this.results.RA) 'h DEC=' num2str(this.results.DEC) ' deg ' l ]);
          end
          
          event_evalfun(mfilename, this.onAnnotated, self, this);
          
          % execute global callback
          event_evalfun(mfilename, self.Callback.onAnnotated, self, this);
          notify(self, 'ANNOTATION_STOP');
          

        else
          % notify/print
          if self.Verbosity > 1
            l = [ '<a href="matlab:web(Telescope,''' this.dir ''');">' this.dir '</a>' ];
            disp([ '[' datestr(now) '] ' class(self) ': ANNOTATION_STOP: ' this.status ' ' l ]);
          end
        end

      end % ~active
    end % if running and java active
  end % for running

end % event_annotate

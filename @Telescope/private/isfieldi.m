function [tf,f] = isfieldi(s, field, varargin)
% ISFIELDI Check if structure contains field, case insensitive.
%
%   [TF, F] = ISFIELDI(struct, field) Returns true when structure contains case
%   insensitive 'field' and returns the corresponding field F and values V.
%
%   F = ISFIELDI(struct, field, 'fieldname') Returns the corresponding fieldname
%   or empty when not found.
%
%   F = ISFIELDI(struct, field, 'value') Returns the corresponding field value
%   of empty when not found.
%
%   struct = ISFIELDI(struct, field, 'set', value) Returns the initial structure
%   with its 'field' modified value.

  tf = false; f='';
  if ~isstruct(s), return; end
  
  fields = fieldnames(s);
  index  = find(strcmpi(field, fields));
  if isscalar(index)
    tf = true;
    f  = fields{index};
  end
  % test additional arguments
  if nargin >=3 && strncmp(varargin{1},'field',5)
    if tf, tf = f; else tf = []; end
  elseif nargin >=3 && strncmp(varargin{1},'value',5)
    if tf, tf = s.(f); else tf = []; end
  elseif nargin >3 && strncmp(varargin{1},'set',3)
    if tf,
      s.(f) = varargin{2};
      tf    = s;
    else tf = s; end
  end
end

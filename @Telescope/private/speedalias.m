function degreesPerSecond = speedalias(self, degreesPerSecond)
% SPEEDALIAS Resolves aliases for speed.
%
%   SPEEDALIAS(S, alias) returns corresponding degrees/sec. Used for Slew and
%   Tracking speeds. The supported aliases are:
%     '1'-'n'     where 'n' is the number of Rate.Slew_Levels
%     'guide' (sidereal),'center','find','max'
%     'sidereal','none' (no tracking),'stop' (stop slewing).

  if isnumeric(degreesPerSecond)
    if     degreesPerSecond < 0
      degreesPerSecond = 0; % stop
    elseif degreesPerSecond > max(self.Rate.Slew_Levels(end))
      degreesPerSecond = max(self.Rate.Slew_Levels(end));
    end
    return
  elseif ischar(degreesPerSecond)
  
    % in case we have a '1'-'max'
    n    = str2double(degreesPerSecond); 
    nmax = numel(self.Rate.Slew_Levels);
    if ~isnan(n)
      n=round(n);
      if n >= 1 && n <= nmax
        degreesPerSecond = self.Rate.Slew_Levels(n);
        return
      else 
        degreesPerSecond = 'center';
      end
    end
    
    switch lower(degreesPerSecond)
    case {'sidereal','stars','guide','slew_guide','track_sidereal'}
      degreesPerSecond = 360/86164.091;
      self.Rate.Track='Track_Sidereal';
    case {'lunar','moon','track_lunar'}
      degreesPerSecond = 14.685/3600; % arcsec -> sec
      self.Rate.Track='Track_Lunar';
    case {'solar','sun','track_solar'}
      degreesPerSecond = 15.000/3600;
      self.Rate.Track='Track_Solar';
    case {'center',num2str(round(nmax/2)),'slew_centering'}
      degreesPerSecond = self.Rate.Slew_Levels(round(nmax/2));
    case {'find',num2str(round(nmax*3/4)),'slew_find' }
      degreesPerSecond = self.Rate.Slew_Levels(round(nmax*3/4));
    case {'max',num2str(nmax),'slew_max'}
      degreesPerSecond = self.Rate.Slew_Levels(end);;
    case {'increase','up','+','out'}
      [~,i] = min(abs(self.Rate.Slew - self.Rate.Slew_Levels));
      if i < nmax, i=i+1; end
      degreesPerSecond = self.Rate.Slew_Levels(i);
    case {'decrease','down','-','in'}
      [~,i] = min(abs(self.Rate.Slew - self.Rate.Slew_Levels));
      if i > 1, i=i-1; end
      degreesPerSecond = self.Rate.Slew_Levels(i);
    case {'none','track_none'}
      self.Rate.Track='';
    case 'stop'
      degreesPerSecond = 0;
    otherwise
      degreesPerSecond = [];
    end
    
  end % ischar
  
end % speedalias

function status = event_slew(self, status, last_ax1, last_ax2)
  
  % are we moving ?
  if last_ax1  ~= self.Coord.Horizontal.ax1 || last_ax2 ~= self.Coord.Horizontal.ax2
    status = 'SLEWING';
    
    % get difference to target
    if   isfield(self.Slew_Target, 'ax1') 
      d_ax1_prev = self.Slew_Target.ax1 -                  last_ax1;
      d_ax1      = self.Slew_Target.ax1 - self.Coord.Horizontal.ax1;
    else d_ax1 = 0; d_ax1_prev = 0; end
    if   isfield(self.Slew_Target, 'ax2')
      d_ax2_prev = self.Slew_Target.ax2 -                  last_ax2;
      d_ax2      = self.Slew_Target.ax2 - self.Coord.Horizontal.ax2;
    else d_ax2 = 0; d_ax2_prev = 0; end
    d_ax1 = mod(d_ax1+180, 360)-180;
    d_ax2 = mod(d_ax2+180, 360)-180;
    d_ax1_prev = mod(d_ax1_prev+180, 360)-180;
    d_ax2_prev = mod(d_ax2_prev+180, 360)-180;
    delta_ax = [ d_ax1 d_ax2 ];
    
    % check target reached: sign change
    if d_ax1*d_ax1_prev < 0 % sign change on axis1: close to target
      speed(self,'ax1',0);  % stop axis1
      d_ax1 = 0;
      if ~isfield(self.Slew_Target, 'ax2'), d_ax2 = 0; end % trigger SLEW REACHED
    end
    if d_ax2*d_ax2_prev < 0 % sign change on axis2: close to target
      speed(self,'ax2',0);  % stop axis2
      d_ax2 = 0; 
      if ~isfield(self.Slew_Target, 'ax1'), d_ax1 = 0; end % trigger SLEW REACHED
    end

    % check target reached: close
    if (abs(d_ax1)  < self.Rate.Slew_Levels(1) && abs(d_ax2) < self.Rate.Slew_Levels(1))
    
      % Close to Slew_Target target. Stop all axes
      status = [ status ' REACHED' ];
      event_evalfun(mfilename, self.Callback.onSlewToReached, self);
      
      if self.Verbosity > 0
        disp([ '[' datestr(now) '] ' class(self) ': SLEW REACHED: RA=' num2str(self.Slew_Target.ra) ' DEC=' num2str(self.Slew_Target.dec) ]);
      end
      self.Slew_Target = [];
      s = self.Rate.Slew; % save previous speed
      speed(self,'ax1',0);  % stop
      speed(self,'ax2',0);
      speed(self, s);       % restore previous speed
      
    elseif ~self.Capabilities.TELESCOPE_CAN_SLEWTO
      % handle automatic slewto speed when not handled by the mount 
      % (only when a Target is defined).
      
      % we should set the speed on given axis to ~min(delta_deg but not 0/Inf)/dt [deg/s]
      delta_ax = delta_ax(delta_ax > 0 & isfinite(delta_ax));
      if ~isempty(delta_ax)
        degreesPerSecond = min(delta_ax)/get(src, 'Period')/2; % deg/s in 2 iterations
        [~,i] = min(abs(degreesPerSecond - self.Rate.Slew_Levels));
        if i == 1, i=2; end % not too slow
        degreesPerSecond = self.Rate.Slew_Levels(i);
        speed(self, degreesPerSecond);
      end

    end
  end % SLEW change
    
end % event_slew

function status = event_capture(self, status, status_prev)
% EVENT_CAPTURE Event loop: Start annotation (onCaptured).

% when AutoAnnotate=true: the onCapture=@annotate is set in the 'image' method.
 
  if ismethod(self.Camera,'ishold') && ishold(self.Camera)
    status = [ status ' CAPTURING' ];
  end
  
  if ~isempty(strfind(status_prev, 'CAPTUR')) && isempty(strfind(status, 'CAPTUR')) ...
  && ~isempty(self.Images) && isstruct(self.Images)
    if exist(self.Images(end).filename,'file')  c=self.Images(end).filename; % was already set ?
    elseif isprop(self.Camera, 'lastImageFile') c=self.Camera.lastImageFile; 
    elseif isprop(self.Camera, 'FileName')      c=self.Camera.FileName; 
    elseif isprop(self.Camera, 'filename')      c=self.Camera.filename; 
    else   c=''; end
    if self.Verbosity > 1
      disp([ '[' datestr(now) '] ' class(self) ': CAPTURE_STOP ' c ]);
    end
    notify(self, 'CAPTURE_STOP');
    self.Images(end).time_stop  = clock;
    self.Images(end).filename   = c;
    
    % check if an image file has been created
    if exist(c, 'file')
      self.Images(end).status   = 'success';
      
      % can annotate only if mount has not moved since image capture start time 
      if isstruct(self.Images(end)) && isfield(self.Images(end),'Coord') ...
      && isfield(self.Images(end).Coord, 'Equatorial')
        ie = self.Images(end).Coord.Equatorial;
      else ie=[]; end
      se = self.Coord.Equatorial;
      
      if isstruct(se) && isstruct(ie) && all([ se.ra se.dec ] == [ ie.ra ie.dec ])
        % auto annotate only if none is already running
        if self.Callback.AutoAnnotate 
          if ~isstruct(self.Annotations) || ~any(strncmp({ self.Annotations.status },'run',3))
            event_evalfun(mfilename, self.Images(end).onCaptured, self, self.Images(end));
          elseif isstruct(self.Annotations) && self.Verbosity > 1
            disp([ '[' datestr(now) '] ' class(self) ': WARNING: Skip annotation (already running)' ]);
          end
        end
        event_evalfun(mfilename, self.Callback.onCaptured,    self, self.Images(end));
      elseif self.Verbosity > 1
        disp([ '[' datestr(now) '] ' class(self) ': WARNING: Skip annotation (mount moved)' ]);
      end
    else
      self.Images(end).status   = 'failed';
    end
  end
 
 end % event_capture

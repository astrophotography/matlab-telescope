function ret=annotate_actions(self, annotations, action)
% ANNOTATE_ACTIONS Perform action on annotations
%
% action can be:
%   LIST
%   STOP
%   REPORT
%   PLOT
%   SUBPLOT
%   UPDATE

  ret=[];
  % UPDATE
  if strfind(action, 'update')
    ret=event_annotate(self);
  end

  % LIST list annotations status
  if strfind(action, 'list')
    ret={};
    for index=1:numel(annotations)
      a = annotations(index);
      [p,f,e] = fileparts(a.filename);
      if isfield(a, 'results') && isstruct(a.results) && isfieldi(a.results,'RA') && isfieldi(a.results,'DEC')
        r=a.results;
        r=[ ' RA=' num2str(r.RA) ' [h] DEC=' num2str(r.DEC) ' [deg]' ];
      else r=''; end
      if isdir(a.dir)
        l = [ '<a href="matlab:web(Telescope,''' a.dir ''');">' a.dir '</a>' ];
      else
        l = 'deleted';
      end
      r=sprintf('%20s %8s %20s %s%s', ...
        datestr(a.time_start), ...
        a.status, ...
        [f e], ...
        l, r);
      if self.Verbosity > 0
        disp(r);
      end
      ret{end+1} = r;
    end
  end
    
  % STOP stop all running processes
  if strfind(action, 'stop')
    running = find(strcmp({annotations.status}, 'running'));
    for index=running
      p = annotations(index).process_java;
      if ~isempty(p) && isjava(p)
        try
          p.destroy;
          if self.Verbosity > 0
            disp([ '[' datestr(now) '] ' mfilename  ': Abort annotation ' annotations(index).dir ])
          end
        end
      end
      annotations(index).process_java = [];
      annotations(index).status = 'failed';
    end
    return
  end
  
  % PLOT/IMAGE Generate image for the reports
  if ~isempty([ strfind(action, 'image') strfind(action, 'plot') ])
  
    annotated = 'annotated.png';
    ret={};
  
    for index=1:numel(annotations)
      a = annotations(index);
      if isfield(a, 'results')
        a=a.results;
      end
      
      if ~exist(fullfile(a.dir, annotated))
        % create annotated image (to appear in md/html)
        fig     = annotate_plot(self, a);
        p       = get(fig, 'Position');
        p(3:4)  = ceil(p(3:4)*1024/p(3));
        set(fig, 'InvertHardcopy','off','Position',p);
        set(fig, 'Visible','off');
        movegui(fig);
        img = getframe(fig);
        imwrite(img.cdata, fullfile(a.dir, annotated));
        close(fig);
        ret{end+1} = fullfile(a.dir, annotated);
      end
        
    end
  end
  
  % PLOT    Generate images from annotations, in separate figures
  % SUBPOLT Generate images from annotations, in a single figure
  if strfind(action, 'plot')
    na  = numel(annotations);
    n   = ceil(sqrt(na)); m = ceil(na/n);
    fig = [];
    ret = [];
    for index=1:na
      % for 'plot', generate subplot
      if strfind(action, 'subplot')
        % a single figure with all annotations as subplots
        if index==1, fig = figure('Name', mfilename); end
        ax = subplot(m,n, index);
        annotate_plot(self, annotations(index), ax);
        ret(end+1) = ax;
      else
        % one figure per annotation
        fig = figure('Name', annotations(index).filename);
        annotate_plot(self, annotations(index), fig);
        ret(end+1) = fig;
      end
    end
  end
  
  % CLEAR   Remove unused images from annotation directories (to avoid filling the disk)
  if strfind(action, 'clea') % ok for CLEAR and CLEAN
    % clear annotation directories, but keep information
    ret={};
    if numel(annotations) > self.Annotations_Max
      for index=1:(numel(annotations)-self.Annotations_Max)
        annotate_actions(self, annotations(index), 'stop');
        rmdir(annotations(index).dir, 's');
        ret{end+1} = annotations(index).dir;
        annotations(index).dir = '';   % we keep information
      end
    end
    % clear unused images (may be large). Keep annotation files and lists.
    notrunning = find(~strcmp({self.Annotations.status}, 'running'));
    for index=notrunning
      delete(fullfile(annotations(index).dir, '*-indx.png'));
      delete(fullfile(annotations(index).dir, '*-objs.png'));
      delete(fullfile(annotations(index).dir, '*.new'));
    end
  end
  
end % annotate_actions

function [X,Y, Az, Alt]=radec2xy(self, varargin)
% RADEC2XY Project Equatorial coordinates using the Stereographic polar projection.
%
%   [X,Y]=RADEC2XY(S, RA,DEC) Converts the [RA,DEC] equatorial to [X,Y] 
%   stereographic polar projection (in -1:1 range), at telescope location and time.
%
%   The RA  can be given in hours   or as a string 'HH:MM:SS'.
%   The DEC can be given in degrees or as a string  DD°MM'SS" or DD*MM:SS.
%   The RA/DEC coordinates can be given as arrays for mass conversion.
%
%   [X,Y]=RADEC2XY(S, RA,DEC, TIME) uses the given UTC TIME.
%   The TIME is the Universal Time Coordinated that can be given as a scalar
%   (e.g. now), a vector [yyyy mm dd HH MM SS] (e.g. clock), or a string (e.g.
%   'dd-mmm-yyyy HH:MM:SS' or 'yyyy-mm-dd HH:MM:SS'). 
%
%   [X,Y]=RADEC2XY(S, ..., Coord) Uses the given longitude/latitude
%   settings (as a struct with 'long','lat' fields).
%
%   [X,Y, AZ,ALT]=RADEC2XY(...) Also returns azimuthal coordinates.
%
% See also: radec2azalt, azalt2radec

  [Az, Alt] = radec2azalt(self, varargin{:});
  [X, Y]    = azalt2xy(self, Az, Alt);
end

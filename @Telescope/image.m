function tf = image(self, varargin)
% IMAGE Take an image with an attached camera.
%
%   IMAGE(S) Takes a single image.
%
%   TF = IMAGE(S) Returns false when the request was unsuccessful (e.g. busy).
%
%   IMAGE(S, ..., 'onCaptured', callback) Triggers the given callback after image 
%   is acquired (after exposure).
%
%   IMAGE(S, ..., 'AutoAnnotate', true|false) Triggers an annotation of the image
%   once acquired.  When not specified, S.Callback.AutoAnnotate is used.
%   IMAGE(S, ..., 'AutoAlign',    true|false) Triggers an alignement of the mount
%   once annotated. When not specified, S.Callback.AutoAlign    is used.
%
%   IMAGE(S, ..., file) Use given image file as if it had been acquired.
%
%   IMAGE(S, 'list') List status of running/past exposures.
%
%   You should first attached a camera object as S.Camera. It should have the 
%   following methods:
%   - ishold   Return true when the camera is busy.
%   - image    Request an image capture.
%   and its last acquired image should be property 'FileName' or 'lastImageFile'.
%   A simulated camera is used when not defined.
    
% this is where autoannotate is set. autoalign can also be set here, or in annotate.
% - autoannotate is handled in 'event' as an onAnnotated callback (event_capture).
% - autoalign    is handled in 'annotate' (update)

  if nargin == 2 && strcmpi(varargin{1},'list')
    tf = image_list(self, self.Images);
    return
  end

  if isempty(self.Camera)
    self.Camera = Camera_simulator(self);
  end
  
  if ~ismethod(self.Camera, 'ishold') || ~ismethod(self.Camera, 'image')
    error([ mfilename ': The attached camera object should have "ishold" and "image" methods.' ]);
  end
  
  % mount and camera are not moving
  [tf, w] = ishold(self);
  if ~tf
    tf = self.Camera.image; % tf=true when image is acquired
  else
    if self.Verbosity > 1
      disp([ '[' datestr(now) '] ' mfilename ': Mount is busy ' w ])
    end
    tf = false;
  end
  if tf
    % add entry in Image list
    this.autoalign    = self.Callback.AutoAlign;    % annotate -> align
    this.autoannotate = self.Callback.AutoAnnotate; % image -> annotate
    this.filename     = '';
    this.index        = numel(self.Images)+1;
    this.method       = class(self.Camera); 
    this.onCaptured   = [];
    this.results      = [];
    this.status       = 'running';
    this.time_start   = clock;
    this.time_stop    = [];
    this.uuid         = tempname;
    this.Coord        = self.Coord; % to check if a move was done during exposure
    [~, this.uuid]    = fileparts(this.uuid);
    f=1;
    while f<= numel(varargin)
      if strncmp(varargin{f},     'onCapt', 6)    && f < numel(varargin)
        this.onCaptured   = varargin{f+1}; f=f+1;
      elseif exist(varargin{f},   'file')
        this.filename     = varargin{f};
      elseif strcmpi(varargin{f}, 'AutoAnnotate') && f < numel(varargin)
        this.autoannotate = varargin{f+1}; f=f+1;
      elseif strcmpi(varargin{f}, 'AutoAlign')    && f < numel(varargin)
        this.autoalign    = varargin{f+1}; f=f+1;
      end
      f=f+1;
    end
    
    % --------------------------------------------------------------------------
    % if Telescope has auto-annotation ON and onCaptured is empty, callback=annotate
    % the annotation is triggered in 'event'.
    if isempty(this.onCaptured) && this.autoannotate
      % if a previous image was done at the same coordinates, we use them as guess
      if ~isempty(self.Images) && isstruct(self.Images(end)) ...
      && isfield(self.Images(end),'Coord') ...
      && isfield(self.Images(end).Coord, 'Equatorial')
        ie = self.Images(end).Coord.Equatorial;
      else ie=[]; end
      se = this.Coord.Equatorial;
      if numel(self.Images) > 1 && isstruct(se) && isstruct(ie) && all([ se.ra se.dec ] == [ ie.ra ie.dec ])
        % mount has not moved, we use its coordinates as guess
        this.onCaptured = @(t,im)annotate(t, im, this.Coord.Equatorial, 'AutoAlign', this.autoalign);
      else
        % blind guess
        this.onCaptured = @(t,im)annotate(t, im, 'AutoAlign', this.autoalign);
      end
    end
    
    if  isempty(self.Images)
      self.Images        = this;
    else
      self.Images(end+1) = this;
    end
    notify(self, 'CAPTURE_START');
    if self.Verbosity > 1
      disp([ '[' datestr(now) '] ' class(self) ': CAPTURE_START ' ]);
    end
    tf = this.uuid;
  end
  
end % image

% ------------------------------------------------------------------------------

function ret=image_list(self, images)
% ANNOTATE_LIST List past acquired images

  ret={};
  for index=1:numel(images)
    a = images(index);
    if isempty(a.filename)
      if index < numel(images), f='skipped'; a.status = 'failed';
      else f='being-acquired'; end
      e='';
    else
      [p,f,e] = fileparts(a.filename);
    end
    if isfield(a, 'results') && isstruct(a.results) && isfield(a.results,'RA') && isfield(a.results,'DEC')
      r=a.results;
      r=[ ' RA=' num2str(r.RA) ' [h] DEC=' num2str(r.DEC) ' [deg]' ];
    else r=''; end
    if exist(a.filename, 'file')
      l = [ '<a href="matlab:web(Telescope,''' a.filename ''');">show</a>' ];
    else l = ''; end
    r=sprintf('%20s %8s %20s %s%s', ...
      datestr(a.time_start), ...
      a.status, ...
      [f e], ...
      l, r);
    if self.Verbosity > 0
      disp(r);
    end
    ret{end+1} = r;
  end
  
end % image_list

function img = getframe(self, ra, dec)
% GETFRAME Get an image of the telescope field-of-view.
%
%   F = GETFRAME returns a snapshot of the current telescope frame as structure 
%   with fields "cdata" and "colormap" which contain the the image data in a  
%   uint8 matrix and the colormap in a double matrix. F.cdata will be 
%   Height-by-Width-by-3 and F.colormap will be empty on systems that use 
%   TrueColor graphics.
%
%   F = GETFRAME(S, RA, DEC) returns a snapshot at given equatorial coordinates.

  if nargin < 3
    ra =self.Coord.Equatorial.ra;   % this is where the mount 'thinks' it is pointing
    dec=self.Coord.Equatorial.dec;
  end

  % generate raw view with only stars
  [~, fig] = plot(self, ra, dec, 'nolabels', 'figure', figure);
  
  % make sure figure is square
  p = get(fig,'position');
  p(3:4) = max(p(3:4));
  set(fig,'position',p+1);
          
  % grab picture
  movegui(fig);
  img = getframe(fig);
  delete(fig);

end % getframe

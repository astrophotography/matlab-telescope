function [tf, w] = ishold(self)
% ISHOLD Return hold state, i.e. true when mount is moving/capturing
%
%   ISHOLD returns true when the mount is moving or capturing a picture.
%
%   [TF,W] = ISHOLD(S) returns the hold state and the reason.

  tf = false; w='IDLE';
  
  if isfield(self.Coord.Target,'active') && self.Coord.Target.active
    tf = true; % GOTO and MOTION
    w  = 'GOTO';
  elseif isfield(self.Slew_Target,'active') && self.Slew_Target.active
    tf = true; % SLEWING
    w  = 'SLEWING';
  elseif ismethod(self.Camera,'ishold') && ishold(self.Camera)
    tf = true; % CAPTURING
    w  = 'CAPTURING';
  end

end

function g=goto(self, varargin)
% GOTO Send mount to given RA/Dec coordinates.
%
%   GOTO(S, RA,DEC) Sends a GOTO request for EQ coordinates.
%   GOTO(S, 'ra',RA,'dec',DEC)
%   The RA is be given in hours and DEC in degrees.
%
%   GOTO(S, 'az',AZ,'alt',ALT) Sends a GOTO request for Azimuthal coordinates.
%   AZ and ALT are given in degrees.
%
%   GOTO(S, 'M 51') Sends a GOTO request for a named object.

% for an auto-center procedure, we expect the following steps:
%
% - goto target
% - image
% - addlistener('captureStop', @annotate) register from camera event
% - wait for event
% - compute difference delta = actual - target (perhaps sph2cart/cart2sph)
% - if any(delta) > threshold:
%   - slew to [actual - delta]
%   - iterate {image, annotate, slew}
% - else if Eq2Mount_T is empty
%   - align (store current EQ and AX coordinates)
%   - if 2 stars and T is empty: T=coords2gps
% - else reached

% TODO: should check that status is IDLE or TRACKING, not CAPTURING
  
  % handle input arguments
  if nargin == 1
    g = self.Coord.Target;
    return
  end
  
  % handle arguments
  [ra,dec,az,alt,ax1,ax2,name] = parseparams(self, varargin{:});

  if isempty([ az alt ra dec ax1 ax2 ]) || any(~isfinite([ az alt ra dec ax1 ax2 ]))
    warning([ mfilename ': ' mfilename ': no target given.' ]);
    return
  end

  % get coordinates to target
  if ~isempty(az) && ~isempty(alt) && isempty([ ra dec ])
    [ra, dec] = azalt2radec(self, az,alt);
  end
  if ~isempty(ax1) && ~isempty(ax2) && isempty([ ra dec ]) ...
  && all(isfinite([ax1 ax2]))
    [ra, dec] = azalt2radec(self, ax1,ax2);
  else
    % compute the mount coordinates, in case this is not azimuthal/equatorial
    [ax1, ax2]= radec2azalt(self, ra, dec);
  end

  % set Target
  g = [];
  g.ra  = ra;
  g.dec = dec;
  g.active = true;
  if ~isempty(az) && ~isempty(alt)
    g.az =az;
    g.alt=alt;
  end
  if ~isempty(ax1) && ~isempty(ax2)
    g.ax1=ax1;
    g.ax2=ax2;
  end
  if ~isempty(name)
    g.name = name;
    name = [ ' > ' name ];
  end

  
  self.Coord.Target = g;
  self.Slew_Target  = [];
  % display message
  if self.Verbosity > 0
    disp([ '[' datestr(now) '] ' class(self) ': ' mfilename ' RA=' num2str(ra) ' DEC=' num2str(dec) name ]);
  end
  notify(self,'GOTO_START');  % STOP in TimerCallback
  notify(self,'MOTION_START');
  % STOP is handled in the timer loop.
  
  % Trigger the move
  % In pratice axis1 ~ RA or Az and axis2 ~ Dec or Alt
  gotoaxis(self, 'ra', ra, 'dec', dec, 'az', az, 'alt', alt);

end % goto

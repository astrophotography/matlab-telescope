function url = web(self, url)
  % WEB Display the StarGo RA/DEC location in a web browser (http://www.sky-map.org).
  %
  %   WEB(S) Shows the Telescope field in sky-map.org
  %
  %   WEB(S, URL) Opens the given URL in a browser.
  
  if nargin < 2
    self.getstatus;
    % get the speed level
    [~,i] = min(abs(self.Rate.Slew - self.Rate.Slew_Levels));
    i = round(i*8/numel(self.Rate.Slew_Levels));
    
    url = sprintf([ 'http://www.sky-map.org/?ra=%f&de=%f&zoom=%d' ...
    '&show_grid=1&show_constellation_lines=1' ...
    '&show_constellation_boundaries=1&show_const_names=0&show_galaxies=1' ], ...
    self.Coord.Equatorial.ra, self.Coord.Equatorial.dec, 9-i);
  end
  % open in system browser
  open_system_browser(url);
end % web

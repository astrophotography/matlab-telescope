function [RA Dec LHA] = mount2radec(self, varargin)
% MOUNT2RADEC Convert mount to Equatorial coordinates.
%
%   [RA,DEC]=MOUNT2RADEC(S, AX1,AX2) Converts the [AX1,AX2] mount to [RA,DEC]
%   equatorial coordinates, at telescope location and time.
%   The AX1 and AX2 angles are given in degrees.
%   The RA is returned in hours, and DEC in degrees.
%
%   [RA,DEC]=MOUNT2RADEC(S, ..., TIME) Uses the given UTC TIME.
%   The TIME is the Universal Time Coordinated that can be given as a scalar
%   (e.g. now), a vector [yyyy mm dd HH MM SS] (e.g. clock), or a string (e.g.
%   'dd-mmm-yyyy HH:MM:SS' or 'yyyy-mm-dd HH:MM:SS'). 
%
%   [RA,DEC]=MOUNT2RADEC(S, ..., Coord) Uses the given longitude/latitude
%   settings (as a struct with 'long','lat' fields).
%
%   Example: for Capella, should return RA='5:18:29' Dec='46°01:6'
%   s.Coord.Geographic=struct('lat',48,'long',2);
%   [RA Dec] = azalt2radec(s, 37.11, 16.18, '22-Feb-2024 10:49:19');
%   { angle2hms(s, RA,'hms') angle2hms(s, Dec, 'dms') }
%
% See also: radec2xy, azalt2radec, radec2altaz, radec2mount
% Reference: http://takitoshimi.starfree.jp/aim/aim.htm

  ax1=[]; ax2=[]; UTC=[]; JD=[];
  RA =[]; Dec=[]; time=[];
  Coord = self.Coord.Geographic;

  % handle input arguments (struct, char, num)
  index=1;
  while index<=numel(varargin)
    s=varargin{index};
    if isstruct(s) && (isfieldi(s, 'ax1') || isfieldi(s, 'ax2'))
      [tf,f]=isfieldi(s, 'ax1'); if tf, ax1=s.(f); end
      [tf,f]=isfieldi(s, 'ax2'); if tf, ax2=s.(f); end
    elseif isstruct(s) && (isfield(s, 'long') && isfield(s, 'lat'))
      Coord = s;
    elseif ischar(s)
      switch s
      case 'ax1'
        if index < numel(varargin), ax1 =varargin{index+1}; index=index+1; end
      case 'ax2'
        if index < numel(varargin), ax2=varargin{index+1}; index=index+1; end
      case 'time'
        if index < numel(varargin), time=varargin{index+1}; index=index+1; end
      otherwise
        time = s; % time as char
      end
    elseif isnumeric(s) && isempty(ax1)
      ax1 = s;
    elseif isnumeric(s) && isempty(ax2)
      ax2 = s;
    elseif isnumeric(s) && isempty(time) && any(numel(s) == [1 6])
      time=s;
    end
    index=index+1;
  end
  
  if isempty(self.Eq2Mount_T) || all(self.Eq2Mount_T(:)==0)
    error([ mfilename ': The mount coordinate frame has not yet been defined. Please point and centre 2 stars.' ])
  end
  
  % handle default input
  if isempty(ax1),  ax1=self.Coord.Horizontal.ax1; end
  if isempty(ax2),  ax2=self.Coord.Horizontal.ax2; end

  ax1=ax1*pi/180;
  ax2=ax2*pi/180;
  
  % compute cosine vector for mount coordinates:
  [x,y,z]   = sph2cart(ax1, ax2, 1);
  
  % compute cosine vector for mount coordinates:
  % T=Eq2Mount_T is the transition matrix between two orthonormal bases.
  % Then it obeys inv(T) = T'
  LMN       = transpose(self.Eq2Mount_T)*[x;y;z];
  
  % revert to angles
  [LHA,Dec] = cart2sph(LMN(1),LMN(2),LMN(3));
  LHA       = LHA*180/pi/15;  % hours
  Dec       = Dec*180/pi;     % degrees
  
  % compute Local Sidereal Time
  [UTC, JD, lst]=date(self, time, Coord); % lst = f(longitude)
  
  % compute local hour angle (LHA)
  % lst = gmst+long
  RA = lst - LHA; % hours

end


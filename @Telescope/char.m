function c = char(self)
  % CHAR Return the mount updated state as a short string.
  c = self.status;
  if isfield(self.Coord.Equatorial,'ra')
    ra = self.Coord.Equatorial.ra;
    dec= self.Coord.Equatorial.dec;
    if isnumeric(ra),  ra =angle2hms(self, ra, 'hms'); end
    if isnumeric(dec), dec=angle2hms(self, dec,'dms'); end
    c = [ c ' RA=' ra ' DEC=' dec ];
  elseif isfield(self.Coord.Horizontal,'alt')
    c = [ c ' AZ='  num2str(self.Coord.Equatorial.az) ...
            ' ALT=' num2str(self.Coord.Equatorial.alt) ];
  end
  if isfield(self.Coord.Target, 'name') && ~strncmp(self.Coord.Target.name,'RA_',3)
    c = [ c ' > ' self.Coord.Target.name ];
  end
  c = [ c ' [' self.Device_Port ']'];
  if self.Verbosity > 1
    c = [ c ' ' self.uid ];
  end
end % char

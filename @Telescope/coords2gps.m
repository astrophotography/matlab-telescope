function [lat long] = coords2gps(self, varargin)
% COORDS2GPS Determine location from Equatorial and Mount/Azimuthal coordinates.
%
%   [LAT,LONG]=COORDS2GPS(S ,RA,DEC, AZ,ALT) Computes the latitude and longitude
%   from the equatorial [RA,DEC] and azimuthal [AZ,ALT] coordinates.
%   This is useful to align/sync a mount.
%   The ALT and AZ angles are given in degrees.
%   The RA  can be given in hours   or as a string 'HH:MM:SS'.
%   The DEC can be given in degrees or as a string DD°MM:SS or DD*MM:SS.
%
%   [T]=COORDS2GPS(S, RA,DEC, AX1,AX2, TIME) Compute the tranformation matrix from
%   equatorial to mount coordinates. All inputs RA,DEC,AX1,AX2,TIME must be arrays
%   or cell arrays of length 2, obtained after aligning two konwn locations (stars). 
%
%   [LAT,LONG]=COORDS2GPS(S, ..., TIME) Uses the given UTC TIME.
%   The TIME is the Universal Time Coordinated that can be given as a scalar
%   (e.g. now), a vector [yyyy mm dd HH MM SS] (e.g. clock), or a string (e.g.
%   'dd-mmm-yyyy HH:MM:SS' or 'yyyy-mm-dd HH:MM:SS'). 
%
%   [LAT,LONG]=COORDS2GPS(S, ..., Coord) Uses the given longitude/latitude
%   settings (as a struct with 'long','lat' fields) as initial guess.
%
%   Example: for Capella, should return [lat=48 long=2]
%   s.Coord.Geographic=struct('lat',48,'long',2); % initial guess
%   [lat long] = coords2gps(s, '5:18:28','46°01:20', 37.11, 16.18, '22-Feb-2024 10:49:19');
  
  % handle input arguments (struct, char, num)
  [ra,dec,az,alt,ax1,ax2,name,lat,long,time] = parseparams(self, varargin{:});
  
  % get current coordinates when not given
  if isempty(ra),  ra =self.Coord.Equatorial.ra;  end % hours
  if isempty(dec), dec=self.Coord.Equatorial.dec; end % deg
  if isempty(az),  az =self.Coord.Horizontal.az;  end % deg
  if isempty(alt), alt=self.Coord.Horizontal.alt; end % deg
  if isempty(ax1), ax1=self.Coord.Horizontal.ax1; end % deg
  if isempty(ax2), ax2=self.Coord.Horizontal.ax2; end % deg
  if isempty(lat), lat=self.Coord.Geographic.lat; end % deg
  if isempty(long),long=self.Coord.Geographic.long; end % deg
  
  if ischar(ra),   ra =hms2angle(self, ra);   end 
  if ischar(dec),  dec=hms2angle(self, dec);  end
  
  if  all(cellfun(@isscalar, {ra,dec,az,alt}))
    [lat, long] = coords2gps_1(self, ra,dec, az,alt, lat, time);
    return
  end
  
  T = coords2gps_2(self, ra,dec, ax1,ax2, time, Coord)
  
  % when used with arrays (multiple reference stars), we may compute a whole 
  % TPOINT-style model to improve pointing accuracy. The initial text states
  % one needs 5-6 minimum, up to ~10 stars to get all parameters.
  % - https://sites.astro.caltech.edu/~srk/TP/P48.pdf
  % There are 7 parameters in the Wallace TPOINT model, so one needs at least
  % the same number of reference stars.
  
  % The Taki model allows to compute coordinate conversion matrices btw
  % mount (AX1,AX2) and equatorial frame (RA,DEC).
  % - http://takitoshimi.starfree.jp/aim/aim.htm
  % K=1.002738
  % we compute the cosine coordinates for each coordinate set. These are the 
  % spherical coordinates on R=1.
  %
  % The Eq mount has a fixed coordinate system (LHA,Dec)
  % The Az                                     (Az, Alt)
  % The sky objects                            (RA, Dec)
  %
  % The advantage of the Taki method is that it does not depend on lat/long, only
  % on a reference time to get LHA (depends on long).
  %
  % The mount coordinates (phi,theta) are extracted from the scope, after centring.
  % These can be mount (LHA,Dec), (Ax1,Ax2) or (Az,Alt). 
  % As we know the target (RA,Dec), we can compute the T matrix from 2 stars.
  % The matrix T contains misalignment errors, as well as axis offsets.
  % In case the mount is purely Eq, then T ~ I.
  % Once we have T, we can convert back and forth, and add other corrections, e.g.:
  %  NP: 6.9    % Non-perpendicularity btw axes
  %  CH: 101.6  % Optical Collimation Error in Azimuth
  %  FO: 73.1   % Flexure in declination
  %
  % NOTE: refraction should be corrected from 'alt' before computing T.
  %
  % mount frame: [l;m;n] = [cosd(alt)*cosd(az);cosd(alt)*sind(az);sind(alt)]
  % Eq    frame: [L;M;N] = [cosd(dec)*cosd(ha);cosd(dec)*sind(ha);sind(dec)]
  %              [l;m;n] = T*[L;M;N]
  %
  % For 2 stars: we get [l1;m1;n1] [L1;M1;N1] and same for star 2.
  % det3 = sqrt((m1*n2-m2*n1)^2+(n1*l2-n2*l1)^2+(l1*m2-l2*m1)^2)
  % [l3;m3;n3] = 1/det3 [m1*n2-m2*n1; n1*l2-n2*l1; l1*m2-l2*m1]
  % T=[l1 l2 l3; m1 m2 m3; n1 n2 n3]*inv([L1 L2 L3; M1 M2 M3; N1 N2 N3])
  %
  % Coordinate changes:
  % (Ra,Dec) -> (LHA,Dec) -> (Ax1,Ax2) scope  | radec2mount / mount2radec
  % (RA,Dec) -> (Az,Alt)                      | radec2altaz / azalt2radec
  

end

% ------------------------------------------------------------------------------

function [lat, long] = coords2gps_1(self, ra,dec, az,alt, lat, time)
  % COORDS2GPS_1 Determine location from single pair (Ra,Dec) (Az,Alt), assumes offsets are 0.
  
  [~, ~, ~, gmst]=date(self, time); % gmst does not depend on longitude
  
  % compute lat=f(alt,az,dec)
  % we start with current lat, and find the closest solution, iteratively
  lat0= lat;
  fun = @(lat)(sind(alt).*sind(lat) + cosd(alt).*cosd(lat).*cosd(az) - sind(dec));
  lat = fzero(fun, lat0);
  if isnan(lat)
    error([ mfilename ': Failed to determine latitude from lat=' num2str(lat0) ]);
  end
  lat = mod(lat, 360);
  
  % compute local hour angle (LHA)
  LHA = atan2(-sind(az).*cosd(alt)./cosd(dec), ...
       (sind(alt)-sind(dec).*sind(lat))./(cosd(dec).*cosd(lat)))*180/pi; % -> LHA in deg
  
  % compute long
  long = (ra - gmst)*15 + LHA; % gmst:hours->degrees, get -> long
  long = mod(long, 360);
end

% ------------------------------------------------------------------------------

function T = coords2gps_2(self, ra,dec, ax1,ax2, time, Coord)
% COORDS2GPS_2 Compute 2-stars coordinate transform matrix from Eq to scope.
%
%   T = COORDS2GPS_2(S, RA,DEC, AX1,AX2) Computes matrix T from a 2-stars align
%   The given coordinates must be 2 elements vectors. The matrix T corresponds 
%   with the transition matrix such as AX = T*EQ (change-of-basis).

  if nargin < 6, time =[]; end
  if nargin < 7, Coord=[]; end
  
  lmn={};
  LMN={};
  if isnumeric(ra)  && numel(ra) == 2, ra =num2cell(ra);  end
  if isnumeric(dec) && numel(dec)== 2, dec=num2cell(dec); end
  if isnumeric(ax1) && numel(ax1)== 2, ax1=num2cell(ax1); end
  if isnumeric(ax2) && numel(ax2)== 2, ax2=num2cell(ax2); end
  if isnumeric(time) && numel(time)== 2, time=num2cell(time); end
  
  for index=1:2
    % cosine vectors for mount coordinates
    [x,y,z]   = sph2cart(ax1{index}, ax2{index}, 1);
    lmn{index}= [x;y;z];
    
    % cosine vectors for Eq coordinates
    if ischar(ra{index}),   ra{index} =hms2angle(self, ra{index});   end 
    if ischar(dec{index}),  dec{index}=hms2angle(self, dec{index});  end
    
    % compute Local Sidereal Time
    [UTC, JD, lst]=date(self, time{index}, Coord); % lst = f(longitude)
    
    % compute local hour angle (LHA)
    % lst = gmst+long
    LHA= lst - ra{index}; % hours = f(longitude)
    
    % compute cosine vector for Eq coordinates:
    [x,y,z]   = sph2cart(LHA*15*pi/180, dec{index}*pi/180, 1);
    LMN{index}= [x;y;z];
  end % for
  
  % complete orthonormal vector unit frame
  % we assume here that the RA/Dec and scope frames are both unary and orthogonal
  % this means that we do not handle deformations, only rotations btw the 2 frames.
  
  lmn{3} = cross(lmn{1}, lmn{2});
  LMN{3} = cross(LMN{1}, LMN{2});

  T=[lmn{1} lmn{2} lmn{3}]*inv([LMN{1} LMN{2} LMN{3}]);
  
end

function [az alt UTC JD] = radec2azalt(self, varargin)
% RADEC2AZALT Convert Equatorial to Azimuthal coordinates.
%
%   [AZ,ALT]=RADEC2AZALT(S, RA,DEC) Converts the [RA,DEC] equatorial to [AZ,ALT] 
%   azimuthal coordinates, at telescope location/orientation and time.
%   The AZ and ALT are returned in degrees.
%
%   The RA  can be given in hours   or as a string 'HH:MM:SS'.
%   The DEC can be given in degrees or as a string  DD:MM:SS or DD°MM'SS".
%   The RA/DEC coordinates can be given as arrays for mass conversion.
%
%   [AZ,ALT]=RADEC2AZALT(S, ..., TIME) Uses the given UTC TIME.
%   The TIME is the Universal Time Coordinated that can be given as a scalar
%   (e.g. now), a vector [yyyy mm dd HH MM SS] (e.g. clock), or a string (e.g.
%   'dd-mmm-yyyy HH:MM:SS' or 'yyyy-mm-dd HH:MM:SS'). 
%
%   [AX1,AX2]=RADEC2AZALT(S, ..., Coord) Uses the given longitude/latitude
%   settings (as a struct with 'long','lat' fields).
%
%   Example: for Capella, should return Az=37.11 Alt=16.18
%   s.Coord.Geographic=struct('lat',48,'long',2);
%   [Az Alt] = radec2azalt(s, '5:18:28','46°01:20','22-Feb-2024 10:49:19');
%
% See also: radec2xy, azalt2radec

  az=[]; alt=[]; UTC=[]; JD=[];
  RA=[]; Dec=[]; time=[];
  Coord = self.Coord.Geographic;
  flag_isEq = false;

  % handle input arguments (struct, char, num)
  index=1;
  while index<=numel(varargin)
    s=varargin{index};
    if isstruct(s) && (isfieldi(s, 'ra') || isfieldi(s, 'dec'))
      [tf,f]=isfieldi(s, 'ra');  if tf, RA =s.(f); end
      [tf,f]=isfieldi(s, 'dec'); if tf, Dec=s.(f); end
    elseif isstruct(s) && (isfield(s, 'long') && isfield(s, 'lat'))
      Coord = s;
    elseif ischar(s)
      switch s
      case 'ra'
        if index < numel(varargin), RA =varargin{index+1}; index=index+1; end
      case 'dec'
        if index < numel(varargin), Dec=varargin{index+1}; index=index+1; end
      case 'time'
        if index < numel(varargin), time=varargin{index+1}; index=index+1; end
      case 'equatorial'
        Coord = self.Coord.Geographic;
        flag_isEq = true;
      case 'azimuthal'
        Coord = self.Coord.Geographic;
      otherwise
        if     isempty(RA),  RA=s;
        elseif isempty(Dec), Dec=s;
        else time = s; % time as char
        end
      end
    elseif isnumeric(s) && isempty(RA)
      RA = s;
    elseif isnumeric(s) && isempty(Dec)
      Dec= s;
    elseif isnumeric(s) && isempty(time) && any(numel(s) == [1 6])
      time=s;
    end
    index=index+1;
  end
  
  % handle default input
  if isempty(RA),  RA =self.Coord.Equatorial.ra;  end
  if isempty(Dec), Dec=self.Coord.Equatorial.dec; end
  
  % test input arguments
  if ischar(RA), RA =hms2angle(self, RA);   end 
  if ischar(Dec),Dec=hms2angle(self, Dec);  end
  
  if any(RA < -48 | RA > 48)
    warning([ mfilename ': The Right Ascension should be given in hours. Odd RA values [' num2str(min(RA(:))) ' ' num2str(max(RA(:))) ']' ]);
  end
  
  if flag_isEq % when Coord = equatorial
    az = RA*15;
    alt= Dec;
    return
  end
  
  % compute Local Sidereal Time
  [UTC, JD, lst]=date(self, time, Coord); % lst = f(longitude)
  
  % compute local hour angle (LHA)
  % lst = gmst+long
  LHA= (lst - RA)*15; % hours -> degrees = f(longitude)

  lat= Coord.lat;
  az = atan2( sind(LHA), cosd(LHA)*sind(lat) - tand(Dec)*cosd(lat) )*180/pi;
  alt= asind( sind(lat)*sind(Dec) + cosd(lat)*cosd(Dec).*cosd(LHA) );
  az = mod(az-180,360);

end


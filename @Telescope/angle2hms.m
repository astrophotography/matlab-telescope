function [h,m,s] = angle2hms(self, ang,in)
  % ANGLE2HMS Convert angle from [deg] to HH:MM:SS
  %
  % ANGLE2HMS(S, ANGLE)          Converts ANGLE [deg]   to DD:MM:SS
  %                              or       ANGLE [hours] to HH:MM:SS
  % ANGLE2HMS(S, ANGLE, option)  Converts ANGLE using options (can be combined):
  %   'hours' Converts ANGLE [deg] to HH:MM:SS (e.g. divides ANGLE by 15).
  %   'hms'   Returns a string HH:MM:SS'
  %   'dms'   Returns a string DD°MM'SS"
  
  if nargin < 2, in=''; end
  if ~isnumeric(ang), h=[]; m=[]; s=[]; end
  if strfind(in, 'hour')
    ang = ang/15;
  end
  sgn=sign(ang); ang=abs(ang);
  h=fix(ang); m=fix(abs(ang-h)*60); s=abs(ang-h-m/60)*3600;
  h=sgn*h; 

  if ~h && sgn==-1, sgn='-'; else sgn=''; end
  if strfind(in, 'hms')
    h = sprintf('%s%d:%02d:%.2f',   sgn, h,m,s);
  elseif strfind(in, 'dms')
    h = sprintf('%s%d°%02d''%.2f"', sgn, h,m,s);
  end
  
end % angle2hms

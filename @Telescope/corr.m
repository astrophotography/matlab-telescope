function [ra,dec,az,alt] = corr(self, varargin)
% CORR Apply corrections to the Equatorial/Azimuthal coordinates.
%
%   [ra,dec]=CORR(S, ra,dec) Computes corrected equatorial coordinates from the
%   ideal input ra/dec and mount misalignment terms. The considered terms are:
%   - Refraction
%   - Flexure
%   - Polar axis azimuth misalignment
%   - Polar axis altitude/elevation misalignment
%   - Non-perpendicularity of the Axes
%   - Empirical terms (constants)
%
% Reference:
% - https://sites.astro.caltech.edu/~srk/TP/Literature/Spillar_WIRO.pdf
% - doi: 10.1299/jsmeicam.2004.4.24_2
% - doi: 10.1117/1.JATIS.4.3.034002
% - http://takitoshimi.starfree.jp/aim/aim.htm
% - https://patents.google.com/patent/US20070115545A1/en
% - https://canburytech.net/DriftAlign/Equations.html

  % handle arguments
  [ra,dec,az,alt,ax1,ax2,name,lat,long,time] = parseparams(self, varargin{:});
  
  % we first compute the local hour angle, azimuth and elevation
  [UTC, JD, lst, gmst]=date(self, time, struct('lat',lat,'long',long)); % lst = f(longitude)
  
  % compute local hour angle (LHA)
  % lst = gmst+long
  LHA = (lst - RA)*15; % hours -> degrees = f(longitude)
  lat = Coord.lat;
  
  % Refraction
  alt = alt + alt_refraction(self, alt);
  
  % https://sites.astro.caltech.edu/~srk/TP/Literature/Wallace_Telescope_Pointing.pdf
  
  % typical values: [arcsec]
  %  ME: 10.2   % misalignment in Elevation
  %  MA:-1.0    % misalignment in Azimuth
  %  NP: 6.9    % Non-perpendicularity btw axes
  %  CH: 101.6  % Optical Collimation Error in Azimuth
  %  FO: 73.1   % Flexure in declination
  %  ID:-13.5   % zero-point errors/offsets
  %  IH: 41.9   % zero-point errors/offsets
  
  
  delta_LHA = tand(dec).*(ME*sind(LHA) - MA*cos(LHA) + NP) ...
            + CH*secd(dec) + IH;
  delta_dec = (ME+FO)*cosd(LHA) + MA*sind(LHA) + ID;
  
  delta_ra  = delta_LHA.*cosd(dec);
end % corr

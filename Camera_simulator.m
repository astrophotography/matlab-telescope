classdef Camera_simulator < handle
% CAMERA_SIMULATOR A camera simulator.
%
%   This class is used when no real camera is attached to the Telescope object.
%     c = Camera_simulator(Telescope)
%
%   The following methods are expected and implemented:
%     IMAGE   Request an image acquisition.
%     ISHOLD  True when the camera is capturing an image.
%   The captured images are generated from the Telescope field-of-view.
%
%   The shutter speed (exposure) can be changed, e.g.
%     c.shutterSpeed  = 2; % in [seconds]
%   The directory to be used to store images is defined as, e.g.
%     c.dirName       = '/tmp';
%   The last acquired image path is available as, e.g.
%     c.FileName        '/tmp/tp18cfb8e5_ff96_4faa_a7b6_40c07f4bd9ec.jpg'

  properties
    shutterSpeed  = 15;       % fake exposure time (s)
    dirName       = tempdir;  % where to store images
  end
  
  properties(SetAccess=protected)
    FileName   = '';  % name of last grabbed picture, empty when busy
    Time_start = [];  % start time for current exposure
  end
  
  properties(Access=protected)
    Active_Devices  ='ACTIVE_TELESCOPE';
    Telescope       = [];  % attached mount
  end
  
  events
    CAPTURE_START % triggered when an acquisition starts
    CAPTURE_STOP  % triggered when an acquisition stops
  end
  
  methods
    function c = Camera_simulator(telescope)
    % CAMERA_SIMULATOR Define a simulated camera.
    %
    %   The associated Telescope object ust be given as argument.
    
      if nargin < 1
        telescope = Telescope; % empty Telescope instance
      end
      disp([ mfilename ': Attaching a Camera simulator to ' class(telescope) ]);
      c.Telescope      = telescope;
      % should also be stored as Telescope.Camera = c;
    end
    
    function tf = image(self, varargin)
    % IMAGE Request an image acquisition.
    %
    %   IMAGE(S) returns true when the image acquisition starts.
    %   The Telescope object will monitor the ishold camera state and detect
    %   when the capture ends.
    
      if ishold(self)
        % an image is already requested (busy)
        disp([ mfilename ': a picture is already requested (busy)...' ]);
        tf = false;
      else
        self.Time_start   = clock;
        self.FileName     = '';
        notify(self, 'CAPTURE_START');
        tf = true;
      end
    end
    
    function tf = ishold(self)
    % ISHOLD True when the camera is capturing an image.
      if isempty(self.FileName) && ~isempty(self.Time_start) && etime(clock, self.Time_start) < self.shutterSpeed
        tf = true;
      else
        tf = false;
        
        % check if an image was just taken
        if isempty(self.FileName)
          % create the fake picture
          % we get the Current Eq coordinates (ideal) and convert to the mount Eq coordinates
          t   = self.Telescope;
          ra  = t.Coord.Equatorial.ra;   % this is where the mount 'thinks' it is pointing
          dec = t.Coord.Equatorial.dec;
          % TODO: add mount misalignment and other perturbations...
          img = getframe(t, ra, dec);
          
          % store image name
          self.FileName   = [ tempname(self.dirName) '.jpg' ];
          imwrite(img.cdata, self.FileName);
        end
        notify(self, 'CAPTURE_STOP');
      end
    end
  end

end % classdef

% ------------------------------------------------------------------------------

function event_evalfun(evt, fun, self, varargin)
% EVENT_EVALFUN evaluate the function callback after event
%   EVENT_EVALFUN(evt, fun, self, ...) Calls 'fun' as a event of type 'evt' (str).
%   The callback should have syntax fun(self, ...)

  if ~isempty(fun) && (isa(fun,'function_handle') || ischar(fun))
    try
      feval(fun, self, varargin{:});
    catch ME
      if isa(fun,'function_handle'), fun=func2str(fun); end
      disp([ mfilename ': ERROR when evaluating ' evt ' ' fun ]);
      getReport(ME)
    end
  end
end

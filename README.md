# matlab-telescope

A base Matlab class to describe a telescope mount.

## Description

This is a base telescope mount class with 'simulator' methods, from which real telescope mount drivers can be derived.

The aim of this class is to provide to all derived mounts some common, advanced functionalities:

- a common behaviour, with uniformed commands and GUIs.
- an automatic set-up of the mount (offsets on both axes).
- an automatic set-up of the location (adjust longitude/latitude to reflect the real mount orientation).
- a real-time identification of the field-of-view.
- a target real-tracking, based on solve-plate of images from attached cameras, and continuous adjustment.

## Usage: simulated mount

When used as is, the `Telescope` class models a simulated mount (in Equatorial settings). 

You should mostly use the `getstatus` (also called when displaying the object), `slew` (move/slew on given axis), `goto` (coordinates or sky object), `speed`, `track` and `stop` methods.

The `image` method takes a an image from the scope field-of-view.
You may define the `t.Camera` property as an object with `ishold` and `image` methods. It should also have a `FileName` property to hold the last acquired image file name.

The `inputdlg` and `plot` methods bring user interfaces.

```matlab
>> t = Telescope

t = Telescope (methods,keypad,more...) TRACKING RA=0:00:0.00 DEC=0°00'0.00" [simulator]

>> slew(t, 'n')
>> t
t = Telescope (methods,keypad,more...) SLEWING RA=0:00:0.00 DEC=0°48'0.00" [simulator]
>> stop(t)
>> goto(t, 'M 51')
findobj: Selecting object M 51 as: M 51;NGC 5194;
  deep_sky_objects: Magnitude: 8.5 ; Type: DSO Gxy ; Dist: 2.8e+07 [ly]
  RA=13:29:52.08 (13.4978 h) Dec=47°11'43.08" (47.1953 deg)

ans =

      ra: 13.4978
     dec: 47.1953
    name: 'M 51;NGC 5194;'
>> getstatus(t)

ans =

GOTO

>> t
t = Telescope (methods,keypad,plot,more...) TRACKING RA=13:29:52.08 DEC=47°11'43.08" > M 51;NGC 5194; [simulator]
>> plot(t)      % sky view of the scope RA/Dec location
>> inputdlg(t)  % key/control pad
>> stop(t)
```

## Usage: subclasses (inheritance)

In practice, you should derive a specific telescope mount from this class with something like:

```matlab
classdef synscan < Telescope

  properties
  end
  
  methods
    % at creation, you may open the connection with 'start'
    
    function delete(self)     % closes
    end
    function s=getstatus(self, ...) % update Telescope_Info, time, coords, and returns its 'status'
    end
    function gotoaxis(self, ...)  % (self, 'ax',deg, ...)
    end
    function slewaxis(self, ...)  % (self, 'ax',deg, ...)
    end
    function s=speed(self, ...) % {deg/s, '1=sidereal','2=center','3=find','4=max','up','down'}
    end                       %
    function start(self)      % initialize, and gets full scope info
    end
    function stop(self)       % stop current move
    end
    function align(self)      % align/sync scope
    end
    function s=track(self, ...) % set tracking speed
    end
  end
  
end
```

which creates a subclass `synscan` with an empty definition for required methods (defined as a simulator in `Telescope`). 
You may have a look at the actual full `synscan` and `Telescope` implementations.

In addition, all derived classes can benefit from the built-in base class GUI's:

- `inputdlg` displays a key-pad to control the mount with basic operations.
- `plot` displays an image of the scope field-of-view around coordinates.
- `annotate` annotates an image and allows to plot its results (see below).

![keypad inputdlg](doc/inputdlg.png?raw=true)

![skyview plot](doc/plot.png?raw=true)

#### Methods to define in derived classes

As shown above, the following methods must be defined in sub-classes of `Telescope` (see example above). 
This way, you shall take into account specific features from mount models.
When not defined, a mount 'simulator' is used.

During subclass creation, you may call the `start` method and grab all mount parameters.

|    Method       | Description                                           |
|-----------------|-------------------------------------------------------|
|    align        | Assigns position to axis (offset)                     |
|    delete       | Closes connection                                     |
|    getstatus    | Update mount status/coord, and returns `status`       |
|    gotoaxis     | Goto mount axis (physical).                           | 
|    slewaxis     | Slew mount axis (physical).                           | 
|    speed        | Sets slew speed `Telescope_Slew_Rate`                 |
|    start        | Initialise connection, and gets full scope info       |
|    stop         | Stop current move                                     |
|    track        | Set tracking speed `Telescope_Track_Rate`             |

The mount can be in different states, which behaviour can be customised with callbacks in `t.Callback`, as follows:

Status    | Description       | Callback          | Events
----------|-------------------|-------------------|--------------------------------
IDLE      | Waiting           | `onUpdate(self)` called at each periodic update | `UPDATED`
TRACKING  | Tracking target   | `onUpdate(self)` called at each periodic update | `UPDATED`
GOTO      | Going to a target, e.g. followed by a tracking | `onGotoReached(self)` when Target is reached after `goto` | `GOTO_START` `GOTO_STOP`
SLEWING   | Slewing/moving to coordinates, but does not affect target | `onSlewReached(self)` when slew target is reached after `slew` | `MOTION_START` `MOTION_STOP`
(CAPTURING) | Camera is taking a picture | `onCaptured(self, image_struct)` when picture is ready | `CAPTURE_START CAPTURE_STOP`
(SOLVED)    | Annotation ends   | `onAnnotate(self, annotation_struct)` when an annotation ends after `annotate` | `ANNOTATION_START ANNOTATION_STOP`

Each callback can be given as a string or as a function handle which first argument is the Telescope object.

It is equally possible to register to associated events with e.g.:
```matlab
addlistener(t, 'GOTO_STOP', @(src,evt)disp('goto reached.'))
```

You should tune the properties in order to describe the mount capabilities, e.g.:

- `t.Capabilities.TELESCOPE_CAN_SLEWTO` to true when the mount can actually slew to a target. All mounts are assumed to be able to slew N/S/E/W. When the capability is `false` (default), the periodic update will adapt the slew speed to reach the target when specified.
- `t.Home_Coord` specifies the starting and HOME location, on both `ax1` and `ax2`.
- `t.Mount_Coord` can be adjusted to describe a non-perfect alignment. This can be computed from a set of (ra,dec) and (ax1,ax2) coordinates with the `coords2gps` method.


## Built-in methods

The `Telescope` class provides standard methods, which are available to all derived subclasses.

Method      | Description
------------|-----------------
angle2hms   | Convert angle from (deg) to HH:MM:SS
annotate    | Solve-plate, i.e. compute coordinates from a sky image.
azalt2radec | Convert Azimuthal to Equatorial coordinates.
azalt2xy    | Project Azimuthal coordinates using the Stereographic polar projection.
char        | Return the mount state as a short string.
coords2gps  | Determine location from Equatorial and Azimuthal coordinates.
date        | Compute local UTC date/time, including the time offset.
display     | Display Telescope object.
event       | Update status/view from timer event.
findobj     | Search for an object (star, DSO, planet) in catalogs.
getframe    | Get an image of the telescope field-of-view.
goto        | Sends the mount to given coordinates.
hms2angle   | Convert hh:mm:ss to an angle in (hour or deg).
home        | Move to Home position e.g. Polar, west or Zenith.
image       | Takes an image at given scope coordinates.
inputdlg    | Display a key pad to control the mount.
ishold      | True when the telescope is moving or capture an image.
location    | Guess the local geographic coordinates (latitude, longitude).
mount2radec | Convert mount to Equatorial coordinates.
plot        | Display an image of the scope field of view, or an annotation.
radec2azalt | Convert Equatorial to Azimuthal coordinates.
radec2mount | Convert Equatorial to mount coordinates.
radec2xy    | Project Equatorial coordinates using the Stereographic polar projection.
reset       | Reset the mount to its start-up state, e.g. `home`,`stop`,`start`
slew        | Slew the mount to given coordinates (`Target_Coord` from `goto` unchanged).
web         | Open sky-map.org at scope coordinates, or URL in browser.

Units: All of Azimuth, Altitude (Elevation), Declination, East Longitude, and North Latitude are in degrees, and RA is given in hours.

The mount coordinates are as follows:

- `t.Equatorial_Coord.ra` and `t.Equatorial_Coord.dec` are the Equatorial coordinates in the sky. These are fixed for each object. When the declination `dec` is 90 degrees, the mount points to the North pole. The Zenith/Apex is at `dec=Geographic_Coord.lat`.
- `t.Horizontal_Coord.az` and `t.Horizontal_Coord.alt` are the Azimuthal coordinates. When `alt` is 90, the scope point to the Zenith (vertical to the geographic location). When `az=0` and `alt=0` the scope points to the North horizon.
- `t.Horizontal_Coord.ax1` and `t.Horizontal_Coord.ax2` are the internal mount coordinates.

The proper way to convert equatorial to azimuthal coordinates and vice-versa are by mean of the `radec2azalt` and `azalt2radec`. This is used e.g. to check if an object is visible, and compute the effect of refraction.

The proper way to convert equatorial to mount coordinates and vice-versa are by mean of the `radec2mount` and `mount2radec`. This is used to e.g. compensate for misalignment and axes offsets.

## Plate-solving (image annotations)

The Telescope class provides a plate-solving capability to all derived classes.

The easiest is to simply call:
```matlab
t = Telescope;
t.annotate('image.png');
```

In case you already know the name of a close-by object, you may use `t.annotate('image.png', findobj(t, 'object_name'));`.

The plate-solve is executed as a background process. If you need to manually update all annotation jobs, call `t.annotate;` (but this is done in the background anyway). 

You may list all past/running annotations with `t.annotate('list');`. All annotations are stored as a structure array `t.Annotations`, and associated data files are in the indicated directories. You may stop all running solve-plate jobs with `t.annotate('stop');`. In addition, you may clear largest files in generated annotation directories with `t.annotate('clear');`.

On success, you may plot the annotated image with:
```matlab
t.annotate('plot');     % one figure per annotation.
t.annotate('subplot');  % all annotations in a single figure.
```
Each annotated image has a Markdown and an HTML report in its results directory.

Last, you may manually generate the annotated image with:
```matlab
t.annotate('image');
```
but this is done as well with the `plot` commands above.

All these commands are also possible when giving a specific set of annotations, e.g.:
```matlab
t.annotate(t.Annotations(end), 'plot'); % plot the last annotation
```

## Aligning mounts

A mount is fully described, in terms of setting-up, by four parameters:

- the offsets on both mechanical axes.
- the mount "vertical" axis, which is usually the Zenith or the polar direction. But this axis can be shifted, e.g. an Equatorial mount could be not perfectly aligned along the polar (Earth) rotation axis, or not fully levelled (for Alt-Azimuthal mounts).

There is no reason to assume that a mount is initially in a particular setting, e.g. Equatorial or Azimuthal. An alignment procedure should not depend on this assumption. 

Most mounts provide an alignment methodology, e.g. to align the Polar star, then point and centre reference stars. This procedure computes the "real" mount orientation and further RA/Dec coordinates are transparently converted to the mount axes values. In this case, any other alignment method is to be avoided. A continuous target tracking correction based on plate-solving can represent a good add-on, but should basically leave the on-board mount pointing software do its job. An aid to align the Polar star or Zenith, and reference stars could represent a nice advantage to reduce the set-up time.

In the case the mount does not provide any pointing mechanism (e.g. a basic Azimuthal mount such as the AZ-GTi), the first step in setting-up a mount is to set define "vertical" and horizontal offsets, e.g. the Altitude (Elevation) or Declination and local hour angle.

Lets review the procedure to align a mount.

- `location(t);` check the (latitude,longitude). This is used further as a starting guess, and to compute the `date(t)`.
- `goto(t, 'ax1',-90,'ax2',85);` bring the scope to e.g. Axis1=-90 (e.g. Az or RA) Axis2=85 (e.g. Alt or Dec, nearly vertical wrt its base).
- `im1=image(t);` take picture, and wait for it.
- `a1=annotate(t, im1);` solve-plate and wait for it.
- `[az1,alt1]=radec2azalt(t, a1.results.RA, a1.results.DEC);` determine RA,Dec and Az,Alt coordinates from the 1st image.
- `[x1,y1,z1]=sph2cart(az1,alt1,1);` compute Cartesian coordinates.
- `goto(t, 'ax1',+90,'ax2',85)` bring Axis1=+90 (Az/RA half turn), leave Axis2 untouched (Alt/Dec).
- `im2=image(t)` take picture, and wait for it.
- `a2=annotate(t, im2)` solve-plate and wait for it.
- `[az1,alt1]=radec2azalt(t, a2.results.RA, a2.results.DEC)` determine RA,Dec and Az,Alt coordinates from the 2nd image.
- `[x2,y2,z2]=sph2cart(az2,alt2,1);` compute Cartesian coordinates.
- `[x3,y3,z3]=mean([x1 y1 z1;x2 y2 z2]);` compute the half-way (Axis1,Axis2) from the 2 absolute coordinates. `[az3,alt3]=cart2sph(x3,y3,z3);`
- `goto(t, 'ax1',az3,'ax2',alt3)` bring the scope vertical wrt its base.
- `align(t, 'ax2',90)` set the vertical axis of the mount as +90.
- compute the direction to the North from the 2 absolute coordinates, set Az=0 (offset/zero). This defines the horizontal plane.
- bring the scope to Az=0,Alt=90
- take picture and solve-plate (determine RA,Dec from the image).
- refine the GPS location using `coords2gps`.

## Credits

(c) E. Farhi 2024, GPL3.

Contributions from:
- Eran Ofek (MAAT) https://github.com/EranOfek/MAAT
- Brian Lau https://www.mathworks.com/matlabcentral/fileexchange/48164-process-manager
- F. Glineur https://fr.mathworks.com/matlabcentral/fileexchange/23393--another--json-parser


